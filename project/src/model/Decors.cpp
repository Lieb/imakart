#include "model/Decors.hpp"
#include <iostream>
#include <glm/ext.hpp>
Decors::Decors()
{
    tMesh = mRock;
    scale = 2.f;
    distanceToCurrentNode = 0.f;

    //physic stuff
    shape = new btBoxShape( btVector3(2.2,0.55,1.1) * scale); // cube
    myTransform.setIdentity();
    myTransform.setOrigin(btVector3(0,11,0) ); // position
    localInertia = btVector3(0,0,0);
    mass = 0.f;
    // calcul de l'inertie si le corps n'est pas statique
    shape->calculateLocalInertia( mass, localInertia );
    myMotionState = new btDefaultMotionState(myTransform);
    //(masse du corps, new btDefaultMotionState, forme géometrique du corps, inertie du corps)
    body_ground = new btRigidBody(mass, myMotionState, shape, localInertia);

}

Decors::Decors(glm::vec3 node, typeMesh mTypeMesh, float sc)
{
    coord = node;
    tMesh = mTypeMesh;
    anim = glm::mat4(1);

    if(tMesh == mRock)
    {
        scale = sc; //1
        myTransform.setOrigin(btVector3(coord.x, coord.y, coord.z) ); // position
        shape = new btBoxShape( btVector3(2.2,1.4,1.75) * scale); // cube
        myMotionState = new btDefaultMotionState(myTransform);
        body_ground = new btRigidBody(0, myMotionState, shape, btVector3(0,0,0));
     }
    //
    else if(tMesh == mTree)
    {
        scale = sc; //0.5
        myTransform.setIdentity();
        myTransform.setOrigin(btVector3(coord.x, coord.y, coord.z) ); // position
        myMotionState = new btDefaultMotionState(myTransform);
        shape = new btBoxShape( btVector3(0.3,0.3,0.3) * scale); // cube
        body_ground = new btRigidBody(0, myMotionState, shape, btVector3(0,0,0));
    }
}

Decors::~Decors()
{
    delete shape;
    delete myMotionState;
    delete body_ground;
}

glm::vec3 Decors::getPosition()
{
    btVector3 pos = body_ground->getWorldTransform().getOrigin();
    return glm::vec3(pos.x(), pos.y(), pos.z());
}

void Decors::setPosition(glm::vec3 position)
{
    btTransform transform = body_ground->getWorldTransform();
    transform.setOrigin(btVector3(position.x, position.y, position.z));
    body_ground->setWorldTransform(transform);
}

glm::mat4 Decors::updateModelView()
{
    glm::mat4 scaleMatrix = glm::scale(glm::mat4(1.0f), glm::vec3(scale));
    //glm::mat4 translateMatrix = glm::translate(glm::mat4(1.0f), coord);
    //glm::mat4 translateMatrix = glm::translate(glm::mat4(1.0f),coord);
    //glm::mat4 translateMatrix2 = glm::translate(glm::mat4(1.0f),glm::vec3(0,-0.5,0) * scale);
    glm::mat4 physicMatrix = glm::make_mat4(matrix);
    return physicMatrix * scaleMatrix;
}

