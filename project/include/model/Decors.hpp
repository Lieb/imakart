/*! \file Decors.hpp
    \brief Implémentations des decors
    \author Clara Baudry, Alexandre Bordereau, Audrey Guénée, Angéline Guignard, Adrien Megueddem, Xavier Vansteene
    \copyright Imakart
    \language C++
    \updated 2013-11-21
*/

#pragma once
#include <string>
#include "graphics/render/Mesh.hpp"
#include <glm/glm.hpp>

/*! \struct Decors
    struct des bonus à intercepter dans le jeu
 */
struct Decors
{

    typeMesh tMesh;
    //std::vector<glm::vec3> coord;
    float scale;
    glm::vec3 coord;
    glm::mat4 anim; /*!< matrice d'animation */

    // physique
    btCollisionShape* shape;    /*!< forme de la boite de physique qui englobe la voiture */
    btTransform myTransform;
    btDefaultMotionState *myMotionState;
    btRigidBody *body_ground;   /*!< type d'objet physique : objet rigide sur lequel va s'appliquer les gravités et collisions */
    btVector3 localInertia;     /*!< inertie de la voiture */
    btScalar mass;              /*!< masse de la voiture */
    btScalar matrix[15];        /*!< matrice de transformation physique */

    float angle;            /*!< angle de rotation en degree autour de l'axe des y */
    float distanceToCurrentNode;

    //Methodes

    Decors();     /*!< constructeur d'un bonus */
    Decors(glm::vec3, typeMesh mTypeMesh, float sc);
    Decors(typeMesh tMesh, std::vector<glm::vec3> coord, float scale);

    ~Decors();

    glm::vec3 getPosition();
    void setPosition(glm::vec3 position);

    glm::mat4 updateModelView();

    std::vector<glm::vec3> setCoord(const std::string path, const std::string type);
};
