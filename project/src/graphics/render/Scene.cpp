#include "graphics/render/Scene.hpp"

Scene::Scene()
{
    program = glimac::loadProgram("./project/src/graphics/shaders/shader.vert", "./project/src/graphics/shaders/shader.frag");
    skyboxProgram = glimac::loadProgram("./project/src/graphics/shaders/shader.vert", "./project/src/graphics/shaders/skybox.frag");
}

Scene::~Scene()
{
}

void Scene::render(GameWorld& model)
{
    skyboxProgram.use();
    // rendu de la skybox
    if(&(model.skybox))
    {
        model.meshs[model.skybox.tMesh]->render(skyboxProgram.getGLId(), model.camera->getViewMatrix()*model.skybox.updateModelView());
    }

    program.use();
    // rendu des lumières et de tous les objets de la scène
    if(&(model.map))
    {
        model.meshs[model.map.tMesh]->render(program.getGLId(), model.camera->getViewMatrix()*model.map.updateModelView());
    }

    //rendu des checkpoints
    for(std::vector<Checkpoint>::iterator itr = model.map.checkpoints.begin(); itr != model.map.checkpoints.end(); ++itr)
    {
        model.meshs[(itr)->tMesh]->render(program.getGLId(), model.camera->getViewMatrix()*(itr)->updateModelView());
    }

    //rendu des arbres
    for(std::vector<Decors*>::iterator itr = model.map.trees.begin(); itr != model.map.trees.end(); ++itr)
    {
        model.meshs[(*itr)->tMesh]->render(program.getGLId(), model.camera->getViewMatrix()*(*itr)->updateModelView());
    }

    //rendu des rochers
    /*for(std::vector<Decors*>::iterator itr = model.map.rocks.begin(); itr != model.map.rocks.end(); ++itr)
    {
        model.meshs[(*itr)->tMesh]->render(program.getGLId(), model.camera->getViewMatrix()*(*itr)->updateModelView());
    }*/

    // Parcours des karts des pilots et ajouts des meshs qu'ils utilisent dans la scène.
    for(std::vector<Pilot*>::const_iterator itr = model.pilots.begin(); itr !=  model.pilots.end(); ++itr)
    {
        model.meshs[(*itr)->kart.tMesh]->render(program.getGLId(), model.camera->getViewMatrix()*(*itr)->kart.updateModelView());
    }
    // Parcours des bonus et ajouts des meshs qu'ils utilisent dans la scène.
    for(std::vector<Bonus*>::iterator itr = model.map.bonus.begin(); itr !=  model.map.bonus.end(); ++itr)
    {
        model.meshs[(*itr)->tMesh]->render(program.getGLId(), model.camera->getViewMatrix()*(*itr)->updateModelView());
    }
}

void Scene::render(GameWorld& model, Map& map)
{
    program.use();
    // rendu des lumières et de tous les objets de la scène
    if(&(map))
    {
       model.meshs[map.tMesh]->render(program.getGLId(), glm::translate(glm::mat4(1), glm::vec3(0.f,0.f,-80.f))*map.updateModelView()*map.anim);
    }

    //rendu des checkpoints
    for(std::vector<Checkpoint>::iterator itr = map.checkpoints.begin(); itr != map.checkpoints.end(); ++itr)
    {
        model.meshs[(itr)->tMesh]->render(program.getGLId(), glm::translate(glm::mat4(1), glm::vec3(0.f,0.f,-80.f))*(itr)->updateModelView()*(itr)->anim);
    }

    //rendu des arbres
    /*for(std::vector<Decors*>::iterator itr = map.trees.begin(); itr != map.trees.end(); ++itr)
    {
        model.meshs[(*itr)->tMesh]->render(program.getGLId(), glm::translate(glm::mat4(1), glm::vec3(0.f,0.f,-5.f))*(*itr)->updateModelView());
    }
        model.meshs[(*itr)->tMesh]->render(program.getGLId(), glm::translate(glm::mat4(1), glm::vec3(0.f,0.f,-80.f))*(*itr)->updateModelView());
    }*/

    //rendu des rochers
    /*for(std::vector<Decors*>::iterator itr = map.rocks.begin(); itr != map.rocks.end(); ++itr)
    {
        model.meshs[(*itr)->tMesh]->render(program.getGLId(), glm::translate(glm::mat4(1), glm::vec3(0.f,0.f,-80.f))*(*itr)->updateModelView());
    }*/
}

void Scene::render(GameWorld& model, std::vector<Pilot*>& pilots)
{
    program.use();
    // Parcours des karts des pilots et ajouts des meshs qu'ils utilisent dans la scène.
    model.meshs[pilots[0]->kart.tMesh]->render(program.getGLId(), glm::translate(glm::mat4(1), glm::vec3(0.f,0.f,-5.f))*pilots[0]->kart.anim);
}

void Scene::render(GameWorld& model, std::vector<Bonus*>& bonus)
{
    program.use();
    // Parcours des karts des pilots et ajouts des meshs qu'ils utilisent dans la scène.
    for(std::vector<Bonus*>::const_iterator itr = bonus.begin(); itr !=  bonus.end(); ++itr)
    {
        model.meshs[(*itr)->tMesh]->render(program.getGLId(), glm::translate(glm::mat4(1), glm::vec3(0.f,0.f,-5.f)));
    }
}

void Scene::render(GameWorld& model, std::vector<Decors*>& decors)
{
    program.use();
    // Parcours des karts des pilots et ajouts des meshs qu'ils utilisent dans la scène.
    for(std::vector<Decors*>::const_iterator itr = decors.begin(); itr !=  decors.end(); ++itr)
    {
        model.meshs[(*itr)->tMesh]->render(program.getGLId(), glm::translate(glm::mat4(1), glm::vec3(0.f,0.f,-5.f)));
    }
}

void Scene::addLight(const Light& light)
{
    lights.push_back(light);
}
