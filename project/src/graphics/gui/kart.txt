Window:
{
	Picture: "Picture1"
	{
        Filename = "third-party/TGUI/widgets/background/imakart/chooseCar.png"
        Left = 0
        Top = 0
        Width = 1024
        Height = 768
        Visible = true
        Enabled = true
        Transparency = 255
        CallbackId = 0
        Smooth = false
	}

    Button: "Button2"
    {
        ConfigFile = "third-party/TGUI/widgets/arrowL.conf"
        Left = 150
        Top = 400
        Width = 48
        Height = 48
        Visible = true
        Enabled = true
        Transparency = 255
        Callback = LeftMouseClicked
        CallbackId = 2
    }

    Button: "Button3"
    {
        ConfigFile = "third-party/TGUI/widgets/arrowR.conf"
        Left = 820
        Top = 400
        Width = 48
        Height = 48
        Visible = true
        Enabled = true
        Transparency = 255
        Callback = LeftMouseClicked
        CallbackId = 3
    }

    Textbox: "TextBox1"
    {
        ConfigFile = "third-party/TGUI/widgets/imakart.conf"
        Left = 350
        Top = 570
        Width = 400
        Height = 200
        Visible = true
        Enabled = false
        Transparency = 255
        CallbackId = 1
        Text = "Buggy\nthe lightest all-terrain car\n\nmax speed ............ 90 km/h\nweight ................... 10 kg\nhandling ................ 3.5/5.0"
        TextSize = 23
        MaximumCharacters = 0
        Borders = (2,2,2,2)
        BackgroundColor = (255,255,255,0)
        TextColor = (0,40,30,255)
        SelectedTextColor = (255,255,255,255)
        SelectedTextBackgroundColor = (0,110,255,255)
        BorderColor = (0,0,0,0)
        SelectionPointColor = (110,110,255,255)
        SelectionPointWidth = 2
    }

    Button: "Button4"
    {
        ConfigFile = "third-party/TGUI/widgets/play.conf"
        Left = 750
        Top = 620
        Width = 120
        Height = 60
        Visible = true
        Enabled = true
        Transparency = 255
        Callback = LeftMouseClicked
        CallbackId = 4
        Text = "Play"
        TextColor = (255,255,255,255)
        TextSize = 20
    }
}
