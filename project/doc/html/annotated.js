var annotated =
[
    [ "Ai", "class_ai.html", "class_ai" ],
    [ "Car", "struct_car.html", "struct_car" ],
    [ "EndState", "class_end_state.html", "class_end_state" ],
    [ "Game", "class_game.html", "class_game" ],
    [ "GameState", "class_game_state.html", "class_game_state" ],
    [ "GameStateManager", "class_game_state_manager.html", "class_game_state_manager" ],
    [ "GraphicalUserInterface", "class_graphical_user_interface.html", "class_graphical_user_interface" ],
    [ "Graphics", "class_graphics.html", "class_graphics" ],
    [ "IntroState", "class_intro_state.html", "class_intro_state" ],
    [ "MenuState", "class_menu_state.html", "class_menu_state" ],
    [ "MysteryBonus", "struct_mystery_bonus.html", "struct_mystery_bonus" ],
    [ "ObjectsManager", "class_objects_manager.html", "class_objects_manager" ],
    [ "Pilot", "struct_pilot.html", "struct_pilot" ],
    [ "PlayState", "class_play_state.html", "class_play_state" ]
];