#include "system/states/LoadState.hpp"

#include "system/GameStateManager.hpp"
#include "system/states/PlayState.hpp"
#include "system/states/RulesState.hpp"

#include <stdio.h>

#include <SFML/Graphics.hpp>

#include "model/GameWorld.hpp"


void LoadState::enter()
{
    printf("enter : LoadState\n");
}

void LoadState::leave()
{
    printf("leave : LoadState\n");

}

void LoadState::pause()
{
    printf("pause : LoadState\n");
}

void LoadState::resume()
{
    printf("resume : LoadState\n");
}

void LoadState::events(GameStateManager &gameStateManager, sf::RenderWindow &window)
{
    sf::Event e;
    while(window.pollEvent(e)) {
        // évènement "fermeture demandée" : on ferme la fenêtre
        if (e.type == sf::Event::Closed)
            window.close();

        switch(e.type) {
            default:
                break;
            case sf::Event::Closed:
                window.close();
                break;

            case sf::Event::KeyPressed:
                switch(e.key.code){
                    case sf::Keyboard::Space:
                        // gameStateManager.changeState(PlayState::instance(window));
                        break;
                }
                break;
        }

        if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
        {
            model.click.play();
        }
        gui.handleEvent(e);
    }

    // TGUI callback loop
    tgui::Callback callback;
    while (gui.pollCallback(callback))
    {
        // Make sure tha callback comes from the button
        if(callback.id == 1)
        {

        }
        else if(callback.id == 2)
        {
                gameStateManager.changeState(PlayState::instance(window, model));
        }
    }
}

void LoadState::update(GameStateManager &gameStateManager, sf::RenderWindow &window)
{
}

void LoadState::draw(sf::RenderWindow &window, int fps)
{
    window.pushGLStates();
    gui.draw();
    window.popGLStates();
}
