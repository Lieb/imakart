Window:
{
	Picture: "Picture1"
	{
                Filename = "third-party/TGUI/widgets/background/imakart/menu.jpg"
                Left = 0
		Top = 0
                Width = 1024
                Height = 768
		Visible = true
		Enabled = true
                Transparency = 255
		CallbackId = 0
		Smooth = false
	}

        Button: "Button1"
        {
                ConfigFile = "third-party/TGUI/widgets/imakart.conf"
                Left = 320
                Top = 250
                Width = 300
                Height = 68
                Visible = true
                Enabled = true
                Transparency = 255
                Callback = LeftMouseClicked
                CallbackId = 1
                Text = "Resume"
                TextColor = (23,124,100,255)
                TextSize = 25
        }

        Button: "Button2"
        {
                ConfigFile = "third-party/TGUI/widgets/imakart.conf"
                Left = 320
                Top = 350
                Width = 300
                Height = 68
                Visible = true
                Enabled = true
                Transparency = 255
                Callback = LeftMouseClicked
                CallbackId = 2
                Text = "Rules"
                TextColor = (23,124,100,255)
                TextSize = 25
        }

        Button: "Button4"
        {
                ConfigFile = "third-party/TGUI/widgets/imakart.conf"
                Left = 320
                Top = 450
                Width = 300
                Height = 68
                Visible = true
                Enabled = true
                Transparency = 255
                Callback = LeftMouseClicked
                CallbackId = 3
                Text = "Restart"
                TextColor = (23,124,100,255)
                TextSize = 25
        }

        Button: "Button4"
        {
                ConfigFile = "third-party/TGUI/widgets/imakart.conf"
                Left = 320
                Top = 550
                Width = 300
                Height = 68
                Visible = true
                Enabled = true
                Transparency = 255
                Callback = LeftMouseClicked
                CallbackId = 4
                Text = "Quit"
                TextColor = (23,124,100,255)
                TextSize = 25
        }

}
