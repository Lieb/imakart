/*! \file GameStateManager.hpp
    \brief Gestionnaire des différents états du jeu sous forme de pile d'états : intro, menu, pause, etc.
    \author Audrey Guénée, Adrien Megueddem
    \copyright Imakart
    \language C++
    \updated 2013-11-24
*/

#pragma once

#include <vector>

#include <SFML/Graphics.hpp>
#include "model/Stopwatch.hpp"

class GameState;

class GameWorld;

/*! \struct GameStateManager
    structure de gestion des états du jeu
    Chaque état implémente les méthodes draw(), update() et events()
 */
class GameStateManager
{
public:
    ~GameStateManager();

    void events(sf::RenderWindow &window);
    void update(sf::RenderWindow &window);
    void draw(sf::RenderWindow &window, int fps);

    void changeState(GameState* state);     /*!< supprime un état de la pile puis le remplace par un autre. */
    int getNbStates() const;                /*!< retourne le nombre d'états de la pile. */
    void pushState(GameState* state);       /*!< ajoute un état sur la pile. */
    void popState();                        /*!< supprime un état sur la pile. */
private:
    // the stack of states
    std::vector<GameState*> states;
};
