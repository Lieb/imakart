#include "model/Kart.hpp"
#include "utils.hpp"
#include <cmath>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/ext.hpp>
#include <iostream>

Kart::Kart()
{
    tMesh = mF1;
    velocity = 0.f;
    acceleration = 0.f;
    rank = 4;
    currentLap = 1;
    scale = 0.25f;
    distanceToCurrentNode = 0.f;
    anim = glm::mat4(1);

    //rotationTimer.start();

    //physic stuff
    shape = new btBoxShape( btVector3(2.2,0.55,1.1) * scale); // cube
    myTransform.setIdentity();
    myTransform.setOrigin(btVector3(0,11,0) ); // position
    localInertia = btVector3(0,0,0);
    mass = 10.f;
    // calcul de l'inertie si le corps n'est pas statique
    shape->calculateLocalInertia( mass, localInertia );
    myMotionState = new btDefaultMotionState(myTransform);
    //(masse du corps, new btDefaultMotionState, forme géometrique du corps, inertie du corps)
    body_ground = new btRigidBody(mass, myMotionState, shape, localInertia);
}

Kart::~Kart()
{
    delete shape;
    delete myMotionState;
    delete body_ground;
}

void Kart::initKart(typeMesh tMesh)
{
    this->tMesh = tMesh;
    if(tMesh == 0)
    {
        acceleration = 0.09; //buggy
        scale = 0.1;
        myTransform.setOrigin(btVector3(0,15,0) ); // position
        shape = new btBoxShape( btVector3(2.2,1.4,1.75) * scale); // cube
        body_ground = new btRigidBody(mass, myMotionState, shape, localInertia);
        description = "Buggy\nthe lightest all-terrain car\n\nmax speed ............ 90 km/h\nweight ................... 300 kg\nhandling ................ 3.5/5.0";
     }
    else if(tMesh == 1)
    {
        acceleration = 0.1; //delorean
        scale = 0.1;
        myTransform.setOrigin(btVector3(0,15,0) ); // position
        shape = new btBoxShape( btVector3(2.75,0.85,1.75) * scale); // cube
        body_ground = new btRigidBody(mass, myMotionState, shape, localInertia);
        description = "DeLorean DMC-12\nthe car back in the future\n\nmax speed ............ 100 km/h\nweight ................... 450 kg\nhandling ................ 3.5/5.0";
    }
    else if(tMesh == 2)
    {
        acceleration = 0.1; //F1
        scale = 0.1f;
        myTransform.setOrigin(btVector3(0,15,0) ); // position
        shape = new btBoxShape( btVector3(2.2,0.55,1.1) * scale); // cube
        body_ground = new btRigidBody(mass, myMotionState, shape, localInertia);
        description = "Formule 1\nthe sportiest car\n\nmax speed ............ 100 km/h\nweight ................... 150 kg\nhandling ................ 3.5/5.0";
    }
    else if(tMesh == 3)
    {
        acceleration = 0.08; //monsterTruck
        scale = 0.1f;
        myTransform.setOrigin(btVector3(0,15,0) ); // position
        shape = new btBoxShape( btVector3(1.5,1.05,1.15) * scale); // cube
        body_ground = new btRigidBody(mass, myMotionState, shape, localInertia);
        description = "Monster Truck\nthe heavy car\n\nmax speed ............ 100 km/h\nweight ................... 500 kg\nhandling ................ 3.5/5.0";
    }
}

void Kart::move()
{
    glm::vec3 vDirectorX = getDirector(glm::vec3(1,0,0));
    setPosition(glm::vec3(vDirectorX.x * velocity + getPosition().x  , vDirectorX.y * velocity + getPosition().y, vDirectorX.z * velocity + getPosition().z));
}

glm::vec3 Kart::getDirector(glm::vec3 axis)
{
    btVector3 vDirector = btVector3(axis.x, axis.y, axis.z);
    vDirector = vDirector.rotate(body_ground->getWorldTransform().getRotation().getAxis(),body_ground->getWorldTransform().getRotation().getAngle());
    //    std::cout << vDirectorX.getX() << " : X get x" << std::endl;
    //    std::cout << vDirectorX.getY() << " : X get y" << std::endl;
    //    std::cout << vDirectorX.getZ() << " : X get z" << std::endl;
    //    std::cout << "///////////////////////////////////" << std::endl;

    return convertBtVectorToGLM(vDirector);
}

bool Kart::isOnCheckpoint()
{
    glm::vec3 coord = getPosition();
    distanceToCurrentNode = sqrt(pow(currentNode.x - coord.x,2) + pow(currentNode.y - coord.y,2) + pow(currentNode.z - coord.z,2));
    if(distanceToCurrentNode < 0.2) // rayon à définir
    {
        return true;
    }
    return false;
}

glm::vec3 Kart::getPosition()
{
    btVector3 pos = body_ground->getWorldTransform().getOrigin();
    return glm::vec3(pos.x(), pos.y(), pos.z());
}

void Kart::setPosition(glm::vec3 position)
{
    btTransform transform = body_ground->getWorldTransform();
    transform.setOrigin(btVector3(position.x, position.y, position.z));
    body_ground->setWorldTransform(transform);
//    btMatrix3x3 orn = body_ground->getWorldTransform().getBasis(); //get basis of world transformation
//    btMatrix3x3 ypr;
//    ypr.se(rotation.x,rotation.y,rotation.z);
//    orn *= ypr; //Multiply it by rotation matrix, yaw, pitch, roll
//    body_ground->getWorldTransform().setBasis(orn); //set new rotation for the object
}

void Kart::setRotation(glm::vec3 rotation)
{
    btMatrix3x3 orn = body_ground->getWorldTransform().getBasis(); //get basis of world transformation
    btMatrix3x3 ypr;
    ypr.setEulerYPR(rotation.x,rotation.y,rotation.z);
    orn *= ypr; //Multiply it by rotation matrix, yaw, pitch, roll
    body_ground->getWorldTransform().setBasis(orn); //set new rotation for the object
}

glm::vec3 Kart::getRotation(){
    glm::vec3 rot;
//    rot.x = body_ground->getOrientation().x();
//    rot.y = body_ground->getOrientation().y();
//    rot.z = body_ground->getOrientation().z();
//    rot.x = body_ground->getWorldTransform().getRotation().x();
//    rot.y = body_ground->getWorldTransform().getRotation().y();
//    rot.z = body_ground->getWorldTransform().getRotation().z();
//    body_ground->getWorldTransform().getBasis().getEulerYPR(rot.x, rot.y, rot.z);
//    body_ground->getWorldTransform().getBasis().getEulerZYX(rot.z, rot.y, rot.x);
//    btTransform transform = body_ground->getWorldTransform();
//    btQuaternion rotation = transform.getRotation();
//    rot.x = rotation.getX();
//    rot.y = rotation.getY();
//    rot.z = rotation.getY();
    btMatrix3x3 m_el = body_ground->getWorldTransform().getBasis();
    rot.x = btAtan2( m_el[0].y(), m_el[0].z() );
    if(rot.x < 0)
        rot.x += 2*M_PI;
    rot.y = btAtan2( m_el[0].z(), m_el[0].x() );
    if(rot.y < 0)
        rot.y += 2*M_PI;
    rot.z = btAtan2( m_el[0].x(), m_el[0].y() );
    if(rot.z < 0)
        rot.z += 2*M_PI;
    return rot;
}

glm::mat4 Kart::updateModelView()
{
    //std::cout << scale << "scale" << std::endl;
    glm::mat4 scaleCar = glm::scale(glm::mat4(1.0f), glm::vec3(scale));
    glm::mat4 rotateCar = glm::rotate(glm::mat4(1.0f), 180.f, glm::vec3(0.f, 1.f, 0.f));
    //glm::mat4 translateMatrix = glm::translate(glm::mat4(1.0f),coord);
    //glm::mat4 translateMatrix2 = glm::translate(glm::mat4(1.0f),glm::vec3(0,-0.5,0) * scale);
    glm::mat4 physicMatrix = glm::make_mat4(matrix);
    return physicMatrix * scaleCar * rotateCar;
}

void Kart::setTypeMeshNext(typeMesh tMesh)
{
    int i = tMesh;
    if(i >= mBuggy && i < mMonsterTruck)
        this->tMesh = static_cast<typeMesh>(++i);
    else
        this->tMesh = mBuggy;

    initKart(this->tMesh);
}

void Kart::setTypeMeshPrev(typeMesh tMesh)
{
    int i = tMesh;
    if(i > mBuggy && i <= mMonsterTruck)
        this->tMesh = static_cast<typeMesh>(--i);
    else
        this->tMesh = mMonsterTruck;

    initKart(this->tMesh);
}


typeMesh Kart::getTypeMesh()
{
    return this->tMesh;
}
