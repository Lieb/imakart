#pragma once

#include "Camera.hpp"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/ext.hpp>

#include "iostream"

#include <model/Kart.hpp>

struct FreeflyCamera : Camera
{
    glm::vec3 m_Position;
    float m_fDistance; // La distance par rapport au centre de la voiture
    float m_fPhi, m_fTheta;
    glm::vec3 m_FrontVector;
    glm::vec3 m_LeftVector;
    glm::vec3 m_UpVector;

    FreeflyCamera(): m_Position(0.f), m_fDistance(0.5), m_fPhi(M_PI), m_fTheta(0.f) {
		computeDirectionVectors();
	}

    FreeflyCamera(glm::vec3& pos, float x, float y): m_Position(0.f), m_fPhi(x), m_fTheta(y) {
        computeDirectionVectors();
    }

    void changeDistance(float delta)
    {
        m_fDistance -= delta;
        m_fDistance = glm::max(0.5f, m_fDistance);
        m_fDistance = glm::min(2.f, m_fDistance);
    }

	void moveFront(float delta) {
		m_Position += m_FrontVector * delta;
	}

	void moveLeft(float delta) {
		m_Position += m_LeftVector * delta;
	}

	void rotateLeft(float degrees) {
		m_fPhi += glm::radians(degrees);
		computeDirectionVectors();
	}

	void rotateUp(float degrees) {
		m_fTheta += glm::radians(degrees);
		computeDirectionVectors();
    }

    void attachCamera(Kart& kart) {
        // position
        m_Position = glm::vec3(kart.getPosition().x, kart.getPosition().y, kart.getPosition().z);

        // front vector troncature
        float Xx = ((int)(kart.getDirector(glm::vec3(1, 0, 0)).x * 100))/100.0;
        float Xy = ((int)(kart.getDirector(glm::vec3(1, 0, 0)).y * 100))/100.0;
        float Xz = ((int)(kart.getDirector(glm::vec3(1, 0, 0)).z * 100))/100.0;

        // up vector troncature
        float Yx = ((int)(kart.getDirector(glm::vec3(0, 1, 0)).x * 100))/100.0;
        float Yy = ((int)(kart.getDirector(glm::vec3(0, 1, 0)).y * 100))/100.0;
        float Yz = ((int)(kart.getDirector(glm::vec3(0, 1, 0)).z * 100))/100.0;

        // left vector troncature
        float Zx = ((int)(kart.getDirector(glm::vec3(0, 0, 1)).x * 100))/100.0;
        float Zy = ((int)(kart.getDirector(glm::vec3(0, 0, 1)).y * 100))/100.0;
        float Zz = ((int)(kart.getDirector(glm::vec3(0, 0, 1)).z * 100))/100.0;

        // vecteurs directeurs
        m_FrontVector = glm::vec3(Xx, Xy, Xz);
        m_LeftVector = glm::vec3(Zx, Zy, Zz);
        m_UpVector = glm::vec3(Yx, Yy, Yz);

        // rotation
        glm::mat4 rotation = glm::rotate(glm::mat4(1.f), -25.f, m_LeftVector);
        m_FrontVector = glm::vec3(rotation * glm::vec4(m_FrontVector, 0));
        m_LeftVector = glm::vec3(rotation * glm::vec4(m_LeftVector,0));
        m_UpVector = glm::vec3(rotation * glm::vec4(m_UpVector,0));

        // translation
        glm::vec3 vectrans = m_FrontVector * -m_fDistance;
        glm::mat4 translation = glm::translate(glm::mat4(1.f), vectrans);
        m_Position = glm::vec3( translation * glm::vec4(m_Position,1));
    }

    void computeDirectionVectors() {
        float PIoverTwo = M_PI * 0.5f;
        float cTheta = cos(m_fTheta);
        m_FrontVector = glm::vec3(cTheta * sin(m_fPhi), sin(m_fTheta), cTheta * cos(m_fPhi));
        m_LeftVector = glm::vec3(sin(m_fPhi + PIoverTwo), 0, cos(m_fPhi + PIoverTwo));
        m_UpVector = glm::cross(m_FrontVector, m_LeftVector);
    }

	glm::mat4 getViewMatrix() const {
		return glm::lookAt(m_Position, m_Position + m_FrontVector, m_UpVector);
    }
};
