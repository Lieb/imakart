#include "system/states/MapState.hpp"

#include "system/GameStateManager.hpp"
#include "system/states/KartState.hpp"

#include <stdio.h>

#include <SFML/Graphics.hpp>

#include "model/GameWorld.hpp"


void MapState::enter()
{
    printf("enter : MapState\n");
    timer.start();
    map.scale = 35.f;
    map.loadConfig("./project/resources/map/map1.map");

}

void MapState::leave()
{
    printf("leave : MapState\n");
}

void MapState::pause()
{
    printf("pause : MapState\n");
}

void MapState::resume()
{
    printf("resume : MapState\n");
}

void MapState::events(GameStateManager &gameStateManager, sf::RenderWindow &window)
{
    sf::Event e;
    while(window.pollEvent(e)) {
        // évènement "fermeture demandée" : on ferme la fenêtre
        if (e.type == sf::Event::Closed)
            window.close();

        switch(e.type) {
            default:
                break;
            case sf::Event::Closed:
                window.close();
                break;

            case sf::Event::KeyPressed:
                switch(e.key.code){
                    case sf::Keyboard::Space:
                        // gameStateManager.changeState(PlayState::instance(window));
                        break;
                }
                break;
        }

        if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
        {
            model.click.play();
        }
        
        gui.handleEvent(e);
    }

    // TGUI callback loop
    tgui::Callback callback;
    while (gui.pollCallback(callback))
    {
        // Make sure tha callback comes from the button
        // Au clic sur la 1ère comboBox
        if(callback.id == 1)
        {
            // Récupération de la map
            tgui::ComboBox::Ptr comboBox1 = gui.get("ComboBox1");
            std::string mapName = comboBox1->getSelectedItem();
            map.setPathFileMap(mapName);
            map.loadConfig(map.pathFileMap);
            //std::cout << map.pathFileMap << std::endl;
        }
        // Au clic sur la 2nde comboBox
        if(callback.id == 2)
        {
            // Récupération de la texture
            tgui::ComboBox::Ptr comboBox2 = gui.get("ComboBox2");
            std::string typeMesh = comboBox2->getSelectedItem();
            map.setMesh(typeMesh);
            //std::cout << map.tMesh << std::endl;
        }
        // Au clic sur "valider"
        if(callback.id == 3)
        {
           // Enregistrement des données choisies dans la structure map du model
           model.map.pathFileMap = map.pathFileMap;
           //loadconfig
           model.map.tMesh = map.tMesh;

           gameStateManager.changeState(KartState::instance(window, model));
        }
    }
}

void MapState::update(GameStateManager &gameStateManager, sf::RenderWindow &window)
{
    int startTime = timer.getElapsedTime().asMilliseconds();
    map.anim = glm::translate(glm::mat4(1), glm::vec3(0.f,0.4f,0.f))*glm::rotate(glm::mat4(1.0f), -startTime*0.05f, glm::vec3(0.0, 1.0, 0.0));

    for(int i = 0; i < map.checkpoints.size(); ++i)
    {
        map.checkpoints[i].anim = glm::translate(glm::mat4(1), glm::vec3(0.f,0.4f,0.f));
        //std::cout << "hey" << std::endl;
    }
}

void MapState::draw(sf::RenderWindow &window, int fps)
{
    window.pushGLStates();
        gui.draw();
    window.popGLStates();
    scene.render(model, map);

    //scene.render(model);
}
