/*! \file ShapeVertex.hpp
    \brief Structure ShapeVertex - glimac
    \author Clara Baudry, Alexandre Bordereau, Audrey Guénée, Angéline Guignard, Adrien Megueddem, Xavier Vansteene
    \copyright Imakart
    \language C++
    \updated 2013-12-19
*/

#pragma once

#include <glm/glm.hpp>

namespace glimac {

    /*! \struct ShapeVertex
        structure d'un ShapeVertex
    */

    struct ShapeVertex {
        glm::vec3 position;
        glm::vec3 normal;
        glm::vec2 texCoords;
    };
}
