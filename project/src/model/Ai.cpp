#include "model/Ai.hpp"
#include "model/Pilot.hpp"
#include <cstdlib>
#include <iostream>

#include <glm/gtx/vector_angle.hpp>
#include <glm/ext.hpp>

#include <cmath>

#include "utils.hpp"

#define EPSILON 0.000001

Ai::Ai()
{
    initRandomKart();
}

Ai::~Ai()
{
}


void Ai::initRandomKart()
{
    int mesh = rand() % 3 + 0;
    kart.initKart((typeMesh) mesh);
}

void Ai::updateDir()
{
    float angle;

    // Get vecDirector to Checkpoint
    glm::vec3 direction;
    direction = kart.currentNode - kart.getPosition();
    direction = glm::normalize(direction);

    // Get frontDirector Kart
    glm::vec3 front = kart.getDirector(glm::vec3(1,0,0));
    front = glm::normalize(front);

    // Transform in 2D
    glm::vec2 frontDir(front.x, front.z);
    glm::vec2 dir(direction.x, direction.z);
    frontDir = glm::normalize(frontDir);
    dir = glm::normalize(dir);

    // Calcul angle
//    float scalar = glm::dot(frontDir, dir);
//    float amplitudeFront = sqrt(pow(frontDir.x ,2) + pow(frontDir.y, 2));
//    float amplitudeDir= sqrt(pow(dir.x ,2) + pow(dir.y, 2));
//    angle = convertRadianToDegree(acos(scalar/(amplitudeFront * amplitudeDir)));

    angle = convertRadianToDegree(atan2(frontDir.x*dir.y - frontDir.y*dir.x, frontDir.x*dir.x - frontDir.y*dir.y));

    std::cout << "angle ai : "<< angle << std::endl;
    std::cout << "yvoiture : "<< direction.y << std::endl;
    std::cout << "yck : "<< front.y << std::endl;

    //Inject angle in rotation by step
    if(angle > 0.f && angle < 180.f)
    {
        kart.setRotation(glm::vec3(0,-0.1,0));
    }
    if(angle < 0.f && angle > -180.f)
    {
        kart.setRotation(glm::vec3(0,0.1,0));
    }
    if(angle < -90.f && kart.velocity > 0.05)
    {
        kart.velocity -= 0.001;
    }
    if(angle > 90.f && kart.velocity > 0.05)
    {
        kart.velocity -= 0.001;
    }
}

void Ai::avoid(glm::vec3 object)
{
    float angle;

    // Get vecDirector to Checkpoint
    glm::vec3 direction;
    direction = object - kart.getPosition();
    direction = glm::normalize(direction);

    // Get frontDirector Kart
    glm::vec3 front = kart.getDirector(glm::vec3(1,0,0));
    front = glm::normalize(front);

    // Transform in 2D
    glm::vec2 frontDir(front.x, front.z);
    glm::vec2 dir(direction.x, direction.z);
    frontDir = glm::normalize(frontDir);
    dir = glm::normalize(dir);

    angle = convertRadianToDegree(atan2(frontDir.x*dir.y - frontDir.y*dir.x, frontDir.x*dir.x - frontDir.y*dir.y));

    //Inject angle in rotation by step
    if(angle > 0.f && angle < 180.f)
    {
        kart.setRotation(glm::vec3(0,0.1,0));
    }
    if(angle < 0.f && angle > -180.f)
    {
        kart.setRotation(glm::vec3(0,-0.1,0));
    }
}

void Ai::useBonus(Pilot *frontPilot)
{
    if(bonus != nullptr)
    {
        if(kart.rank > 1)
        {
            bonus->active(this, frontPilot);
        }
    }
}

void Ai::updateMove()
{
    if(kart.velocity < kart.acceleration)
    {
        kart.velocity += 0.0005;
    }
    else if(kart.velocity > kart.acceleration || (kart.velocity - kart.acceleration) < EPSILON)
    {
       kart.velocity = kart.acceleration;
    }
    kart.move();
}
