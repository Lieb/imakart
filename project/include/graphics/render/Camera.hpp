#pragma once

#include <glm/glm.hpp>

#include "iostream"

struct Kart;

struct Camera {

    virtual void changeDistance(float delta) = 0;
    virtual void moveFront(float delta) = 0;
    virtual void rotateUp(float degrees) = 0;
    virtual void rotateLeft(float degrees) = 0;

    virtual void attachCamera(Kart& kart) = 0;

    virtual glm::mat4 getViewMatrix() const = 0;
};
