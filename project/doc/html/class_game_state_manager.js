var class_game_state_manager =
[
    [ "~GameStateManager", "class_game_state_manager.html#ad20961eb5d9ed6b7ef764037d5e11ba5", null ],
    [ "changeState", "class_game_state_manager.html#ad5aad336dd3aa967595d5cdc9329318a", null ],
    [ "draw", "class_game_state_manager.html#a0989a6e0a0b8c80bb9976e108950bd52", null ],
    [ "events", "class_game_state_manager.html#ac374d2832ae07d31a190ebfd69c30d53", null ],
    [ "getNbStates", "class_game_state_manager.html#a3dd0683288019da097a656ea14d19a60", null ],
    [ "popState", "class_game_state_manager.html#abc41a4fbaab083c1182452273e5e06c7", null ],
    [ "pushState", "class_game_state_manager.html#ae6e74676682cc425bbaecf812c8283af", null ],
    [ "update", "class_game_state_manager.html#ac434d9625e23322308ed0cf8fe41957c", null ]
];