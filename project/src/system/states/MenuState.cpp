#include "system/states/MenuState.hpp"

#include "system/GameStateManager.hpp"
#include "system/states/InformationsState.hpp"
#include "system/states/RulesState.hpp"

#include <stdio.h>

#include <SFML/Graphics.hpp>

#include "model/GameWorld.hpp"


void MenuState::enter()
{
    printf("enter : MenuState\n");
    model.menus.setVolume(50);
    model.menus.play();
}

void MenuState::leave()
{
    printf("leave : MenuState\n");
    model.menus.stop();
}

void MenuState::pause()
{
    printf("pause : MenuState\n");
}

void MenuState::resume()
{
    printf("resume : MenuState\n");
}

void MenuState::events(GameStateManager &gameStateManager, sf::RenderWindow &window)
{
    sf::Event e;
    while(window.pollEvent(e)) {
        // évènement "fermeture demandée" : on ferme la fenêtre
        if (e.type == sf::Event::Closed)
            window.close();

        switch(e.type) {
            default:
                break;
            case sf::Event::Closed:
                window.close();
                break;

            case sf::Event::KeyPressed:
                switch(e.key.code){

                }
                break;
        }

        if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
        {
            model.click.play();
        }
        
        gui.handleEvent(e);
    }

    // TGUI callback loop
    tgui::Callback callback;
    while (gui.pollCallback(callback))
    {
        // Make sure tha callback comes from the button
        if(callback.id == 1)
        {
                gameStateManager.changeState(InformationsState::instance(window, model));
        }
        else if(callback.id == 2)
        {
                gameStateManager.pushState(RulesState::instance(window, model));
        }
        else if(callback.id == 3)
        {
                window.close();
        }
    }
}

void MenuState::update(GameStateManager &gameStateManager, sf::RenderWindow &window)
{
}

void MenuState::draw(sf::RenderWindow &window, int fps)
{
    window.pushGLStates();
    gui.draw();
    window.popGLStates();
}
