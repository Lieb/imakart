/*! \file Stop.hpp
    \brief Implémentations du bonus de turbo
    \author Clara Baudry, Alexandre Bordereau, Audrey Guénée, Angéline Guignard, Adrien Megueddem, Xavier Vansteene
    \copyright Imakart
    \language C++
    \updated 2013-11-21
*/

#pragma once
#include "model/Bonus.hpp"

/*! \class Stop
    classe du bonus stop à intercepter dans le jeu
 */
class Stop: public Bonus
{
    glm::mat4 anim; /*!< matrice d'animation */

public:

    Stop(glm::vec3 coord, typeMesh tMesh, std::string name,  int duration, float scale, int etat,std::string imageGUI);      /*!< constructeur d'un bonus */
    ~Stop();
    void draw();
    virtual void active(Pilot *pilot, Pilot *pilotfront);   /*!< activation du stop à un Pilot */
};
