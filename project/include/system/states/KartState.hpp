/*! \file KartState.hpp
    \brief Etat de menu du jeu qui permet de sélectionner son kart.
    \author Audrey Guénée, Adrien Megueddem
    \copyright Imakart
    \language C++
    \updated 2013-11-24
*/

#pragma once

#include "system/states/GameState.hpp"

#include <stdexcept>

#include <SFML/Graphics.hpp>

#include <TGUI/TGUI.hpp>

#include "graphics/render/Scene.hpp"
#include "model/Pilot.hpp"
#include <vector>

class GameStateManager;

class GameWorld;

/*! \struct KartState
    structure de l'état de selection de kart.
    singleton pour qu'un état ne soit créé qu'une seule fois et reste unique.
 */
class KartState : public GameState
{
public:
    virtual void enter();
    virtual void leave();

    virtual void pause();
    virtual void resume();

    virtual void events(GameStateManager& gameStateManager, sf::RenderWindow &window);
    virtual void update(GameStateManager& gameStateManager, sf::RenderWindow &window);
    virtual void draw(sf::RenderWindow &window, int fps);

    static KartState* instance(sf::RenderWindow& window, GameWorld& model)
    {
        static KartState kart(window, model);
        return &kart;
    }
protected:
    KartState(sf::RenderWindow& window, GameWorld& model) :gui(window), model(model)
    {
        // Initialisation de la GUI
        if(gui.setGlobalFont("third-party/TGUI/fonts/DejaVuSans.ttf") == false) throw std::runtime_error("KartState::Failure to open ttf with TGUI");
        // Load all the widgets from a file
        gui.loadWidgetsFromFile("project/src/graphics/gui/kart.txt");
    }
    GameWorld& model;
    tgui::Gui gui;
    Scene scene;
    std::vector<Pilot*> pilots;
    StopWatch timer;

};
