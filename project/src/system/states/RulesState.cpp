#include "system/states/RulesState.hpp"

#include "system/GameStateManager.hpp"

#include <stdio.h>

#include <SFML/Graphics.hpp>

#include "model/GameWorld.hpp"


void RulesState::enter()
{
    printf("enter : RulesState\n");
}

void RulesState::leave()
{
    printf("leave : RulesState\n");
}

void RulesState::pause()
{
    printf("pause : RulesState\n");
}

void RulesState::resume()
{
    printf("resume : RulesState\n");
}

void RulesState::events(GameStateManager &gameStateManager, sf::RenderWindow &window)
{
    sf::Event e;
    while(window.pollEvent(e)) {
        // évènement "fermeture demandée" : on ferme la fenêtre
        if (e.type == sf::Event::Closed)
            window.close();

        switch(e.type) {
            default:
                break;
            case sf::Event::Closed:
                window.close();
                break;

            case sf::Event::KeyPressed:
                switch(e.key.code){
                    case sf::Keyboard::Space:
                        // Do something
                        break;
                }
                break;
        }

        if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
        {
            model.click.play();
        }
        
        gui.handleEvent(e);
    }

    // TGUI callback loop
    tgui::Callback callback;
    while (gui.pollCallback(callback))
    {
        // Make sure tha callback comes from the button
        if(callback.id == 1)
        {
                gameStateManager.popState();
        }
    }
}

void RulesState::update(GameStateManager &gameStateManager, sf::RenderWindow &window)
{
}

void RulesState::draw(sf::RenderWindow &window, int fps)
{
    window.pushGLStates();
    gui.draw();
    window.popGLStates();
}
