/*! \file EndState.hpp
    \brief Etat de fin de jeu qui permet de mettre d'afficher les crédits ou un menu de fin.
    \author Audrey Guénée, Adrien Megueddem
    \copyright Imakart
    \language C++
    \updated 2013-11-24
*/

#pragma once

#include "system/states/GameState.hpp"

#include <stdexcept>

#include <SFML/Graphics.hpp>

#include <TGUI/TGUI.hpp>

#include "graphics/render/Scene.hpp"

class GameWorld;

class GameStateManager;

/*! \struct EndState
    structure de l'état de fin du jeu.
    singleton pour qu'un état ne soit créé qu'une seule fois et reste unique.
 */
class EndState : public GameState
{
public:
    virtual void enter();
    virtual void leave();

    virtual void pause();
    virtual void resume();

    virtual void events(GameStateManager& gameStateManager, sf::RenderWindow &window);
    virtual void update(GameStateManager& gameStateManager, sf::RenderWindow &window);
    virtual void draw(sf::RenderWindow &window, int fps);

    static EndState* instance(sf::RenderWindow& window, GameWorld& model)
    {
        static EndState end(window, model);
        return &end;
    }
protected:
    EndState(sf::RenderWindow& window, GameWorld& model):gui(window), model(model)
    {
        // Initialisation de la GUI
        if(gui.setGlobalFont("third-party/TGUI/fonts/run.ttf") == false) throw std::runtime_error("EndState::Failure to open ttf with TGUI");
        // Load all the widgets from a file
        gui.loadWidgetsFromFile("project/src/graphics/gui/end.txt");
    }
    GameWorld& model;
    tgui::Gui gui;
    Scene scene;
};
