# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/clara/ImaKart/Imakart/third-party/TGUI/src/TGUI/FormBuilder/Form.cpp" "/home/clara/ImaKart/Imakart/third-party/TGUI/build/src/TGUI/FormBuilder/CMakeFiles/FormBuilder.dir/Form.cpp.o"
  "/home/clara/ImaKart/Imakart/third-party/TGUI/src/TGUI/FormBuilder/FormBuilder.cpp" "/home/clara/ImaKart/Imakart/third-party/TGUI/build/src/TGUI/FormBuilder/CMakeFiles/FormBuilder.dir/FormBuilder.cpp.o"
  "/home/clara/ImaKart/Imakart/third-party/TGUI/src/TGUI/FormBuilder/MenuBar.cpp" "/home/clara/ImaKart/Imakart/third-party/TGUI/build/src/TGUI/FormBuilder/CMakeFiles/FormBuilder.dir/MenuBar.cpp.o"
  "/home/clara/ImaKart/Imakart/third-party/TGUI/src/TGUI/FormBuilder/main.cpp" "/home/clara/ImaKart/Imakart/third-party/TGUI/build/src/TGUI/FormBuilder/CMakeFiles/FormBuilder.dir/main.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "WIDGETS_FOLDER=\"/usr/local/share/tgui-0.6/widgets\""
  "IMAGES_FOLDER=\"/usr/local/share/tgui-0.6/form-builder-data/images\""
  "DATA_FOLDER=\"/usr/local/share/tgui-0.6/form-builder-data\""
  "FONTS_FOLDER=\"/usr/local/share/tgui-0.6/fonts\""
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/clara/ImaKart/Imakart/third-party/TGUI/build/src/TGUI/CMakeFiles/tgui.dir/DependInfo.cmake"
  )
