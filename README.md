# README

IMAKART est un projet de jeu vidéo de course de voitures utilisant OpenGL et SFML.

------------------------------------------------------------

## Guide d'installation

/!\ Le jeu se compile et s'exécute dans le dossier '/build'.

### CMake
Il est souhaitable de créer le dossier build du CMake à la racine d'imakart.
Il sera automatiquement ignorer par git grâce au .gitignore mis en place.

### OpenGL
	sudo apt-get install freeglut3-dev

### SFML
Pour pouvoir utiliser TGUI il faut avoir installer auparavant sfml 2.x
Cette version n'est pas disponible sur apt-get install sfml
Il faut donc le télécharger http://www.sfml-dev.org/download.php en prenant la version "tout OS"
Ensuite installer tous les paquets de dépendances avant de compiler sfml :

- pthread
- opengl
- xlib
- xrandr
- freetype
- glew
- jpeg
- sndfile
- openal

http://sfml-dev.org/tutorials/2.0/compile-with-cmake-fr.php#installer-les-dependances

Une fois toutes les lib installées on peut créer le build grâce à Cmake.
Ensuite dans le terminal pour pouvoir installer les librairies sfml dans les dossiers locaux : 

	sudo make install 

Bravo ! c'est fini pour la sfml

### TGUI
Téléchargez l'archive TGUI-master
https://github.com/texus/TGUI/archive/master.zip
Dans le dossier cmake lancer :

	cmake ..

Ensuite compilez TGUI grâce à la commande :

	make

Et voila, plus qu'à compiler notre projet à partir du dossier build en utilisant la commande :

	cmake ..

------------------------------------------------------------

## Architecture du projet
	// Répertoire build
	/build

	// Jeu
        /project
            // Documents texte relatifs au projet
            /doc

            // Médias utilisés (objets, images, sons, etc.)
            /resources
                /map
                /obj
                /sound
                /textures
		
            // Fichiers .hpp
            /include
                // Rendu graphique
                /graphics
                    /debug
                    /glimac
                    /render
                    render.hpp
                // Objets (voitures, obstacles, etc.)
                /model
                // Etats du jeu
                    /system
                        /states
                        Game.hpp
                        GameStateManager.hpp
                // Fonctions utilitaires
                utils.hpp
		
            // Fichiers .cpp
            /src
                // Rendu graphique
                /graphics
                    /debug
                    /glimac
                    /render
                    /gui
                    /shaders
                // Objets (voitures, obstacles, etc.)
                /model
                // Etats du jeu
                    /system
                        /states
                        Game.cpp
                        GameStateManager.cpp
                // Fonctions utilitaires
                utils.cpp
		
            CMakeLists.txt

            // Fichier de l'exécution principale du jeu (contrôleur général)
            main.cpp

        // Librairies externes (tierce-partie)
        /third-party
            /assimp-3.0.1270
            /bullet-2.82
            /glew-1.10.0
            /glm
            /SFML-2.1
            /TGUI

	// CMake principal
	CMakeLists.txt

-------------------------------------

## Styles sheet

### Structures et classes
	:::magpie
	class nomClasse 
	{
		public:
			// Constructeur
			nomClasse();
			// Destructeur
			~nomClasse();
			
			// Surchage d'opérateurs
			type& operator[](type arg);
			const type& operator[](type arg) const;
			
			// Méthodes
			type nomFonction() const;
			
		private:
			// Attributs
			type nomAttribut;
	};

- Placer 'public' avant 'private'
- Notation "camelCase"

### Fonctions
	:::magpie
	void nomFonction(int val1, int val2)
	{
		for(unsigned int i = 0; i < 100; ++i)
		{
			if(i <= 50) 
			{
				// code
			}
		}
	}

- Première accolade toujours à la ligne
- Indentation du code entre les accolades (fonctions, boucles, tests, etc.)
- Notation "camelCase"

#### Constructeurs et destructeurs
	:::magpie
	nomClasse::nomClasse(type arg1, type arg2) :
					attribut1(arg1), attribut2(arg2) 
	{
	}
	
	ParticleVector::~ParticleVector() 
	{
    	delete[] m_pParticles;
	}

### Opérateurs
Référence, copie, pointeur (`*` et `&`), collés au type lors de la déclaration
	  
	  int* ptr = NULL;
	  
Référence, copie, pointeur (`*` et `&`), collés au nom lors de l'utilisation
	  
	  *ptr = &var;

Espaces entre les opérateurs :  `=`  `+`  `-`  `*`  `/`
	
	:::magpie
	float res = (a + b) * (a - b) / 5;

### Variables
- Déclaration de variables spécifiques uniquement au moment où on en a besoin (compteurs, par exemple)
- Déclaration de variables souvent utilisées en début de fonction
- Noms de variables explicites et pertinentes
- Notation "camelCase"

### Commentaires et documentation (Doxygen)
	/*! \brief Brief description.
	 *         Brief description continued.
	 *
	 *  Detailed description starts here.
	 */

- Commentaires dans les .h
- Pas de commentaires dans les .c, sauf si besoin

### Commits sur Bitbucket
	// Mises à jour
	UP:	 __	+ fonction + fichier + commentaires
	// Ajouts ou modifications de fichier
	ADD: __	+ fonction + fichier + commentaires
	// Suppression de fichier
	DEL: __	+ fonction + fichier + commentaires
	// Merges ou résolution de bugs
	FIX: __	+ fonction + fichier + commentaires

#### Rappels sur GIT
	git pull
	git status
	git add _____.cpp ou git rm _____.cpp
	git commit -m "ADD: _____"
	git pull
	git push
	git checkout -- _____.cpp (annule un git "add _____.cpp")

	/* Branches */
	// Création d'une branche
	git branch nom_branche
	// Placement sur la branche
	git checkout nom_branche
	// Retour sur la branche master
	git checkout master
	// Fusion de la branche avec master
	git merge master
	// Suppression de la branche
	git branch -d nom_branche
	// Pour forcer la suppression si la branche n'a pas été merge
	git branch -D nom_branche
	// Liste de ses branches
	git branch -v
	// Liste détaillée de ses branches
	git branch 

### Pour travailler sur la branche
- se positionner sur la branche en question
- add, commit et push ses modifications

### Pour merger la branche à la master
- se positionner sur la branche en question
- faire le merge master

### Pour supprimer la branche :
- se positionner sur la master
- supprimer la branche en question
