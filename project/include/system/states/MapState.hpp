/*! \file MapState.hpp
    \brief Etat de selection de maps du jeu qui permet de sélectionner les différentes maps du jeu.
    \author Audrey Guénée, Adrien Megueddem
    \copyright Imakart
    \language C++
    \updated 2013-11-24
*/

#pragma once

#include "system/states/GameState.hpp"

#include <stdexcept>

#include <SFML/Graphics.hpp>

#include <TGUI/TGUI.hpp>

#include "graphics/render/Scene.hpp"

class GameStateManager;

class GameWorld;

/*! \struct MapState
    structure de l'état de selection de map du jeu.
    singleton pour qu'un état ne soit créé qu'une seule fois et reste unique.
 */
class MapState : public GameState
{
public:
    virtual void enter();
    virtual void leave();

    virtual void pause();
    virtual void resume();

    virtual void events(GameStateManager& gameStateManager, sf::RenderWindow &window);
    virtual void update(GameStateManager& gameStateManager, sf::RenderWindow &window);
    virtual void draw(sf::RenderWindow &window, int fps);

    static MapState* instance(sf::RenderWindow& window, GameWorld& model)
    {
        static MapState map(window, model);
        return &map;
    }
protected:
    MapState(sf::RenderWindow& window, GameWorld& model) : gui(window), model(model)
    {
        //rotationTimer.start();
        // Initialisation de la GUI
        if(gui.setGlobalFont("third-party/TGUI/fonts/run.ttf") == false) throw std::runtime_error("MapState::Failure to open ttf with TGUI");
        // Load all the widgets from a file
        gui.loadWidgetsFromFile("project/src/graphics/gui/map.txt");
    }
    GameWorld& model;
    tgui::Gui gui;
    Scene scene;
    Map map;
    StopWatch timer;
};
