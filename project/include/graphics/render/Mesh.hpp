#pragma once

#include <GL/glew.h>
#include <cstdlib>
#include <string>
#include <vector>

#include "graphics/glimac/ShapeVertex.hpp"
#include "graphics/glimac/Program.hpp"

#include "utils.hpp"

#include <assimp/Importer.hpp>
#include <assimp/scene.h> // Output data structure
#include <assimp/mesh.h>
#include <assimp/postprocess.h> // Post processing flags

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <GL/glew.h>

enum typeMesh
{
    mBuggy,
    mDelorean,
    mF1,
    mMonsterTruck,
    mSandWorld,
    mGrassWorld,
    mSnowWorld,
    mSkybox,
    mFlower,
    mRock,
    mTree,
    mRoad,
    mBonus,
    mNode,
    mCheckpoint
};

struct Mesh
{
    // Variables uniformes (+/-)
    GLint locationUniformMVPMatrix;
    GLint locationUniformMVMatrix;
    GLint locationUniformNormalMatrix;
    GLint locationUniformTextureMatrix;

    // Tableau de vertices (positions, normales, coordonnées textures)
    std::vector<glimac::ShapeVertex> vertices;

    // VAO & VBO
    GLuint vbo;
    GLuint vao;

    //Texture
    GLuint texture;

    Mesh(std::string pathObj, std::string pathTexture);
    Mesh(std::vector<glm::vec3> nodes);
    ~Mesh();

    //load un .obj, stock les normals, les coordonées de texture et les coordonnées de position
    bool loadObj(const std::string path);

    //load le tableau de noeud d'un fichier .map
    bool loadNodes(std::vector<glm::vec3> nodes);


    //on dessine
    void render(GLuint prog, glm::mat4 trans, GLenum typeDraw=GL_TRIANGLES);
};
