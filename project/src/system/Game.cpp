#include "system/Game.hpp"

#include <iostream>
#include <ctime>

#include <SFML/Graphics.hpp>

#include "system/GameStateManager.hpp"
#include "system/states/IntroState.hpp"
#include "model/GameWorld.hpp"
#include "model/Stopwatch.hpp"

Game::Game() :
    FPS(30), WINDOW_WIDTH(1024), WINDOW_HEIGHT(768), FRAMERATE_MILLISECONDS(1000/30)
{
    window.create(sf::VideoMode(WINDOW_WIDTH, WINDOW_HEIGHT), "Polymakart", sf::Style::Fullscreen);

    glewCode = glewInit();
}

int Game::init()
{
    std::cout << "Init Game" << std::endl;

    //Démarrage de l'horloge globale
    timer.start();

    if(GLEW_OK != glewCode) {
        std::cerr << "Unable to initialize GLEW : " << glewGetErrorString(glewCode) << std::endl;
        //throw std::exception("OpenGl can't be initialized correctly");
        return (EXIT_FAILURE);
    }

    glEnable(GL_DEPTH_TEST);

    //Initialisation de la graine pour les random du jeu
    srand (time(NULL));

    //Initialisation du model.
    model.initWorld();


    // Mise en place du système d'état du jeu
    statesManager.pushState(IntroState::instance(window, model));
    std::cout << "end init Game" << std::endl;
    return 0;


}

void Game::run()
{
    //Calcul des temps pour régler l'horloge globale
    sf::Time startTime =   sf::milliseconds(0);
    sf::Time elapsedTime =  sf::milliseconds(0);
    sf::Time elapsedTimeForASecond =  sf::milliseconds(0);
    sf::Time framerate = sf::milliseconds(FRAMERATE_MILLISECONDS);
    int framesInstant, framesPerSecond;

    while(window.isOpen())
    {
        startTime = timer.getElapsedTime();

        // Nettoyage de la fenêtre
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        window.clear(sf::Color(255,255,255));

        // loop Event code goes here
        statesManager.events(window);

        // Application code goes here
        statesManager.update(window);

        //Calcul des FPS
        framesInstant ++;

        if(elapsedTimeForASecond.asMilliseconds() >= 1000)
        {
            framesPerSecond = framesInstant-1;
            framesInstant = 0;
            elapsedTimeForASecond = sf::milliseconds(0);
        }


        // Rendering code goes here
        statesManager.draw(window, framesPerSecond);

        // Mise à jour de la fenêtre (synchronisation implicite avec OpenGL)
        window.display();

        //Réglage horloge globale du jeu
        elapsedTime = timer.getElapsedTime() - startTime;

        if(elapsedTime.asMilliseconds() < FRAMERATE_MILLISECONDS)
        {
           sf::sleep(framerate - elapsedTime);
        }

        elapsedTimeForASecond += (timer.getElapsedTime() - startTime);




    }
}

