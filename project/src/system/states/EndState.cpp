#include "system/states/EndState.hpp"

#include "system/GameStateManager.hpp"
#include "system/states/MenuState.hpp"

#include <stdio.h>

#include <SFML/Graphics.hpp>

#include "model/GameWorld.hpp"

void EndState::enter()
{
    printf("enter : EndState\n");
    model.credits.setVolume(50);
    model.credits.play();
}

void EndState::leave()
{
    printf("leave : EndState\n");
    model.credits.stop();
}

void EndState::pause()
{
    printf("pause : EndState\n");
}

void EndState::resume()
{
    printf("resume : EndState\n");
}

void EndState::events(GameStateManager &gameStateManager, sf::RenderWindow &window)
{
    sf::Event e;
    while(window.pollEvent(e)) {
        // évènement "fermeture demandée" : on ferme la fenêtre
        if (e.type == sf::Event::Closed)
            window.close();

        switch(e.type) {
            default:
                break;
            case sf::Event::Closed:
                window.close();
                break;

            case sf::Event::KeyPressed:
                switch(e.key.code){
                    case sf::Keyboard::Space:
                        // Do something
                        break;
                }
                break;
        }
        gui.handleEvent(e);
    }

    // TGUI callback loop
    tgui::Callback callback;
    while (gui.pollCallback(callback))
    {
        // Make sure tha callback comes from the button
        if(callback.id == 1)
        {
                gameStateManager.changeState(MenuState::instance(window, model));
        }
    }
}

void EndState::update(GameStateManager &gameStateManager, sf::RenderWindow &window)
{
}

void EndState::draw(sf::RenderWindow &window, int fps)
{
    window.pushGLStates();
    gui.draw();
    window.popGLStates();
}
