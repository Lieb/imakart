#include "model/Timer.hpp"
#include "assert.h"

Timer::Timer() : _clock(), _timeLimit(sf::Time::Zero)
{

}

Timer::Timer(sf::Time timeLimit, bool initRunning) : _clock(initRunning), _timeLimit(timeLimit)
{
    assert(timeLimit > sf::Time::Zero);
}

Timer::~Timer()
{

}

sf::Time   Timer::getRemainingTime()      const
{
    // Le temps restant est soit zéro, soit le temps limite - le temps écoulé.
    // Si la soustraction donne un résultat négatif, zéro sera plus grand, sinon on renvoie le résultat de cette soustraction
    return std::max((_timeLimit - _clock.getElapsedTime()), sf::Time::Zero);
}

bool    Timer::isRunning()             const
{
    return (_clock.isRunning() && !isExpired());
}

bool    Timer::isExpired()             const
{
    return (_clock.getElapsedTime() > _timeLimit);
}

void    Timer::start()
{
    _clock.start();
}

void    Timer::stop()
{
    _clock.stop();
}

void    Timer::restart(sf::Time timeLimit, bool continueRunning)
{
    assert(timeLimit > sf::Time::Zero);

    _timeLimit = timeLimit;
    _clock.restart(continueRunning);
}

void    Timer::restart(bool continueRunning)
{
    _clock.restart(continueRunning);
}
