#pragma once

#include "Camera.hpp"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "model/Kart.hpp"

#include "utils.hpp"


struct TrackballCamera : Camera
{
    glm::vec3 m_Position;
    float m_fDistance; //la distance par rapport au centre de la voiture
    float m_fAngleX; //l'angle effectuée par la caméra autour de l'axe x de la voiture (rotation vers le haut ou vers le bas)
    float m_fAngleY; //l'angle effectuée par la caméra autour de l'axe y de la voiture (rotation vers la gauche ou vers la droite)

    TrackballCamera() : m_fDistance(3), m_Position(0,0,0), m_fAngleX(0), m_fAngleY(0)
	{}

    TrackballCamera(glm::vec3 pos, float d, float x, float y) : m_Position(pos.x,pos.y,pos.z), m_fDistance (d), m_fAngleX (x), m_fAngleY (y)
	{}

    ~TrackballCamera()
	{}

    virtual void moveFront(float delta)
    {
        m_fDistance -= delta;
        m_fDistance = glm::max(0.5f, m_fDistance);
        m_fDistance = glm::min(50.f, m_fDistance);
    }

    void changeDistance(float delta)
	{
        m_fDistance -= delta;
        m_fDistance = glm::max(0.5f, m_fDistance);
        m_fDistance = glm::min(50.f, m_fDistance);
    }

    void rotateLeft(float degrees)
	{
	    m_fAngleY +=degrees;
	}

    void rotateUp(float degrees)
	{
	    m_fAngleX +=degrees;
	}

    void attachCamera(Kart& kart) {
        m_Position = kart.getPosition();
//        btMatrix3x3 rot = kart.body_ground->getOrientation().getAngle();
//        btScalar yaw, pitch, roll;
//        rot.getEulerZYX(yaw, pitch, roll);
        m_fAngleX = convertRadianToDegree(kart.body_ground->getOrientation().getAngle())-30;
        m_fAngleY = convertRadianToDegree(kart.body_ground->getOrientation().getAngle())-90;
//        m_fAngleY = convertRadianToDegree(kart.getRotation().y)-90;
    }

    glm::mat4 getViewMatrix() const{
        glm::mat4 ViewMatrix = glm::mat4(1);
        ViewMatrix = glm::translate(ViewMatrix, glm::vec3(0, 0, -m_fDistance));
        ViewMatrix = glm::rotate(ViewMatrix,-(TrackballCamera::m_fAngleX), glm::vec3(1,0,0));
        ViewMatrix = glm::rotate(ViewMatrix, -(TrackballCamera::m_fAngleY), glm::vec3(0,1,0));
        ViewMatrix = glm::translate(ViewMatrix, glm::vec3(-m_Position.x, -m_Position.y, -m_Position.z));

	    return ViewMatrix;
	}
};



