#include "system/states/LooseState.hpp"

#include "system/GameStateManager.hpp"
#include "system/states/EndState.hpp"

#include <stdio.h>

#include <SFML/Graphics.hpp>

#include "model/GameWorld.hpp"


void LooseState::enter()
{
    printf("enter : LooseState\n");
    model.loose.setVolume(50);
    model.loose.play();
    timer.start();
    pilots.push_back(new Pilot);
    pilots[0]->kart.initKart(model.pilots[0]->kart.tMesh);
}

void LooseState::leave()
{
    printf("leave : LooseState\n");
    std::cout << "clear gameworld" << std::endl;
}

void LooseState::pause()
{
    printf("pause : LooseState\n");
}

void LooseState::resume()
{
    printf("resume : LooseState\n");
}

void LooseState::events(GameStateManager &gameStateManager, sf::RenderWindow &window)
{
    sf::Event e;
    while(window.pollEvent(e)) {
        // évènement "fermeture demandée" : on ferme la fenêtre
        if (e.type == sf::Event::Closed)
            window.close();

        switch(e.type) {
            default:
                break;
            case sf::Event::Closed:
                window.close();
                break;

            case sf::Event::KeyPressed:
                switch(e.key.code){
                    case sf::Keyboard::Space:
                        // Do something
                        break;
                }
                break;
        }

        if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
        {
            model.click.play();
        }
        
        gui.handleEvent(e);
    }

    // TGUI callback loop
    tgui::Callback callback;
    while (gui.pollCallback(callback))
    {
        // Make sure tha callback comes from the button
        if(callback.id == 1)
        {
                gameStateManager.changeState(EndState::instance(window, model));
        }
    }
}

void LooseState::update(GameStateManager &gameStateManager, sf::RenderWindow &window)
{
    int startTime = timer.getElapsedTime().asMilliseconds();
    pilots[0]->kart.anim = glm::translate(glm::mat4(1), glm::vec3(0.f,6.f,-20.f))*glm::rotate(glm::mat4(1.0f), startTime*0.05f, glm::vec3(0.0, 1.0, 0.0));
}

void LooseState::draw(sf::RenderWindow &window, int fps)
{
    window.pushGLStates();
    gui.draw();
    window.popGLStates();
    scene.render(model, pilots);
}
