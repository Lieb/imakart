/*! \file Skybox.hpp
    \brief Implémentations d'une skybox
    \author Clara Baudry, Alexandre Bordereau, Audrey Guénée, Angéline Guignard, Adrien Megueddem, Xavier Vansteene
    \copyright Imakart
    \language C++
    \updated 2013-12-28
*/

#pragma once

#include <string>
#include <glm/glm.hpp>

#include "btBulletDynamicsCommon.h"
#include "graphics/render/Mesh.hpp"

#include "model/Stopwatch.hpp"

/*! \struct Skybox
    Structure d'une skybox
 */
struct Skybox
{
    typeMesh tMesh;     /*!< type de mesh : mSkybox */
    glm::vec3 coord;    /*!< coordonnées de la skybox */
    float scale;        /*!< taille/échelle de la skybox */
    glm::mat4 anim;     /*!< matrice d'animation */

    StopWatch rotationTimer;    /*!< timer de rotation de la skybox */

    Skybox();   /*!< constructeur d'une skybox */
    ~Skybox(); /*!< destructeur d'une skybox */

    glm::mat4 updateModelView();

};
