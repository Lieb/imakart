/*! \file Ai.hpp
    \brief Implémentations de l'intelligence artificielle des autres joueurs.
    \author Clara Baudry, Alexandre Bordereau, Audrey Guénée, Angéline Guignard, Adrien Megueddem, Xavier Vansteene
    \copyright Imakart
    \language C++
    \updated 2013-11-07
*/

#pragma once
#include <vector>
#include "model/Pilot.hpp"

/*! \class Ai
    classe du pilote "robot" du jeu
 */
class Ai: public Pilot
{
public:
    //std::vector<Coord coord> node;
    Ai();
    ~Ai();

    void initRandomKart();

    void updateDir();
    void avoid(glm::vec3 object);
    void useBonus(Pilot* frontPilot);
    void updateMove();
};
