/*! \file GameState.hpp
    \brief Classe mère étendue par les autres états du jeu.
    \author Audrey Guénée, Adrien Megueddem
    \copyright Imakart
    \language C++
    \updated 2013-11-24
*/

#pragma once

#include <SFML/Graphics.hpp>

#include "model/GameWorld.hpp"

#include <sstream>

class GameStateManager;

/*! \struct GameState
    structure mère des différents états du jeu
    singleton pour qu'un état ne soit créé qu'une seule fois et reste unique.
 */
class GameState
{
public:
    virtual void enter() = 0;
    virtual void leave() = 0;

    virtual void pause() = 0;
    virtual void resume() = 0;

    virtual void events(GameStateManager& gameStateManager, sf::RenderWindow &window) = 0;
    virtual void update(GameStateManager& gameStateManager, sf::RenderWindow &window) = 0;
    virtual void draw(sf::RenderWindow &window, int fps) = 0;
protected:
    GameState(){}
    std::stringstream ssFPS;
    std::stringstream ssVitesse;
    std::stringstream ssPos;
    std::stringstream ssLap;
};
