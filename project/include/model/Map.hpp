/*! \file Map.hpp
    \brief Implémentations de la carte 3D (terrain) du jeu.
    \author Clara Baudry, Alexandre Bordereau, Audrey Guénée, Angéline Guignard, Adrien Megueddem, Xavier Vansteene
    \copyright Imakart
    \language C++
    \updated 2013-12-16
*/

#pragma once
#include <vector>
#include <iostream>
#include <string>
#include "btBulletDynamicsCommon.h"
#include "graphics/render/Mesh.hpp"
#include "utils.hpp"
#include "model/Checkpoint.hpp"
#include "model/Bonus.hpp"
#include "model/Decors.hpp"
#include "model/SlowDown.hpp"
#include "model/Stop.hpp"
#include "model/Turbo.hpp"
#include "model/Swap.hpp"

#include <glm/glm.hpp>

/*! \struct Map
    structure du terrain de jeu (carte 3D)
 */
struct Map
{
    typeMesh tMesh;
    std::string pathFileMap;        /*!< chemin d'acces au fichier .map recenssant les infos de la map */
    glm::vec3 coord;                /*!< coordonnées de la position de la map */
    float scale;
    std::vector<Checkpoint> checkpoints;

    std::vector<Decors*> trees;
    std::vector<Decors*> rocks;

    glm::mat4 anim;                 /*!< matrice d'animation */

    std::vector<Bonus*> bonus;

    //mapCoordinates mCoord;
    //std::vector<glm::vec3> mCoord; // vecteur des coordonnées des nodes

    // physique
    btCollisionShape* shape;    /*!< forme de collision physique */
    btTransform myTransform;
    btDefaultMotionState *myMotionState;
    btRigidBody *body_ground;   /*!< rigidBody englobant la map */

    Map();   /*!< constructeur du terrain */
    ~Map();  /*!< destructeur du terrain */

    void loadConfig(const std::string path);
    glm::mat4 updateModelView();

    void setPathFileMap(std::string map);
    void setMesh(std::string type);
};


