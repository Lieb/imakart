#include "system/states/PlayMenuState.hpp"

#include "system/GameStateManager.hpp"
#include "system/states/RulesState.hpp"
#include "system/states/MenuState.hpp"

#include <stdio.h>

#include <SFML/Graphics.hpp>

#include "model/GameWorld.hpp"


void PlayMenuState::enter()
{
    printf("enter : PlayMenuState\n");
    model.menus.setVolume(50);
    model.menus.play();
}

void PlayMenuState::leave()
{
    printf("leave : PlayMenuState\n");
    model.menus.stop();
}

void PlayMenuState::pause()
{
    printf("pause : PlayMenuState\n");
}

void PlayMenuState::resume()
{
    printf("resume : PlayMenuState\n");
}

void PlayMenuState::events(GameStateManager &gameStateManager, sf::RenderWindow &window)
{
    sf::Event e;
    while(window.pollEvent(e)) {
        // évènement "fermeture demandée" : on ferme la fenêtre
        if (e.type == sf::Event::Closed)
            window.close();

        switch(e.type) {
            default:
                break;
            case sf::Event::Closed:
                window.close();
                break;

            case sf::Event::KeyPressed:
                switch(e.key.code){
                    case sf::Keyboard::Space:
                        // Do something
                        break;
                }
                break;
        }

        if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
        {
            model.click.play();
        }
        
        gui.handleEvent(e);
    }

    // TGUI callback loop
    tgui::Callback callback;
    while (gui.pollCallback(callback))
    {
        // Make sure tha callback comes from the button
        if(callback.id == 1)
        {
            gameStateManager.popState();
        }
        if(callback.id == 2)
        {
            gameStateManager.pushState(RulesState::instance(window, model));
        }
        if(callback.id == 3)
        {
            // Nettoyer le model avant de recommencer une partie.
            gameStateManager.changeState(MenuState::instance(window, model));
        }
        if(callback.id == 4)
        {
            window.close();
        }
    }
}

void PlayMenuState::update(GameStateManager &gameStateManager, sf::RenderWindow &window)
{
}

void PlayMenuState::draw(sf::RenderWindow &window, int fps)
{
    window.pushGLStates();
    gui.draw();
    window.popGLStates();
}
