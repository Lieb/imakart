/*! \file WinState.hpp
    \brief Etat gagné de jeu qui permet d'e mettre d'afficher les crédits ou un menu de fin'annoncer au joueur sa victoire.
    \author Audrey Guénée, Adrien Megueddem
    \copyright Imakart
    \language C++
    \updated 2013-11-24
*/

#pragma once

#include "system/states/GameState.hpp"

#include <stdexcept>

#include <SFML/Graphics.hpp>

#include <TGUI/TGUI.hpp>

#include "graphics/render/Scene.hpp"

class GameStateManager;

class GameWorld;

/*! \struct WinState
    structure de l'état gagné du jeu.
    singleton pour qu'un état ne soit créé qu'une seule fois et reste unique.
 */
class WinState : public GameState
{
public:
    virtual void enter();
    virtual void leave();

    virtual void pause();
    virtual void resume();

    virtual void events(GameStateManager& gameStateManager, sf::RenderWindow &window);
    virtual void update(GameStateManager& gameStateManager, sf::RenderWindow &window);
    virtual void draw(sf::RenderWindow &window, int fps);

    static WinState* instance(sf::RenderWindow& window, GameWorld& model)
    {
        static WinState win(window, model);
        return &win;
    }
protected:
    WinState(sf::RenderWindow& window, GameWorld& model) : gui(window), model(model)
    {
        // Initialisation GUI
        if(gui.setGlobalFont("third-party/TGUI/fonts/run.ttf") == false) throw std::runtime_error("WinState::Failure to open ttf with TGUI");
        // Load all the widgets from a file
        gui.loadWidgetsFromFile("project/src/graphics/gui/win.txt");
    }
    GameWorld& model;
    tgui::Gui gui;
    Scene scene;
    std::vector<Pilot*> pilots;
    StopWatch timer;
};
