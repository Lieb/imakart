#pragma once

#include <SFML/System.hpp>

class StopWatch
{
    public:
        /**
        * @brief StopWatch constructor
        */
        StopWatch(bool initrunning = false);

        /**
        * @brief Virtual default destructor
        */
        virtual ~StopWatch();

        /**
        * @brief Return the elapsed time since the start or last reset
        * @return Elapsed time
        */
        sf::Time getElapsedTime() const;

        /**
        * @brief Is the StopWatch running ?
        * @return true if it's running
        */
        bool isRunning() const;

        /**
        * @brief Start the StopWatch
        */
        void start();

        /**
        * @brief Stop the StopWatch
        */
        void stop();

        /**
        * @brief Reset the StopWatch
        * @param stillrunning If true, the StopWatch will keep running, by default it stops !
        */
        void restart(bool stillrunning = false);

    private:
        sf::Clock   _clock;
        sf::Time    _buffer;
        bool        _running;
};
