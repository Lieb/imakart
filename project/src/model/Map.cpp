#include "model/Map.hpp"
#include <glm/ext.hpp>

Map::Map()
{
    tMesh = mGrassWorld;
    pathFileMap = "./project/resources/map/map1.map";
    scale = 70.f;

    //physic stuff
    shape = new btSphereShape(0.5007 * scale);
    myTransform.setIdentity();
    myTransform.setOrigin(btVector3(0,0,0)); //position
    myMotionState = new btDefaultMotionState(myTransform);

    body_ground = new btRigidBody(0,myMotionState, shape, btVector3(0,0,0));
    body_ground->setGravity(btVector3(0,0,0));

}

Map::~Map()
{
    delete body_ground;
    delete myMotionState;
    delete shape;

    for (int i = 0; i < bonus.size(); ++i)
    {
        delete bonus[i];
    }
    bonus.clear();
}

void Map::loadConfig(const std::string path)
{
    //road = getSphericalCoordinates(path, 1);

    // si le fichier est correct
    if(parser(path)) {
        float r = scale*0.5003; // rayon de la sphère à définir
        // Récupération de toutes les coordonnées du .map
        mapCoordinates allCoord = getSphericalCoordinates(path, r);
        // Stockage des coordonnées des nodes uniquement (dans un vector de vec3)

        // Vidage du vecteur checkpoint
        checkpoints.clear();
        for(int i=0; i < allCoord["nodes"].size(); ++i){
            Checkpoint ck(allCoord["nodes"][i], scale/100);
            checkpoints.push_back(ck);
        }
        // for bonus
        // Vidage du vecteur bonus
        bonus.clear();
        for(int i=0;i<allCoord["nodes"].size(); ++i){

            int typeRandom = rand() % 4 + 1;
            std::cout << "Type du bonus" << i << " : "<< typeRandom << std::endl;

            if(typeRandom == 1){
                std::cout << "Type de bonus 1" << std::endl;
                bonus.push_back((new Turbo(allCoord["nodes"][i], (typeMesh)12, "Turbo", 2000, 0.1, 0, "third-party/TGUI/widgets/pictures/gift.png")));
            }
            if(typeRandom == 2){
                std::cout << "Type de bonus 2" << std::endl;
                bonus.push_back((new Swap(allCoord["nodes"][i], (typeMesh)12, "Swap", 10000, 0.1, 0, "third-party/TGUI/widgets/pictures/gift.png")));
            }
            if(typeRandom ==3){
                std::cout << "Type de bonus 3" << std::endl;
                bonus.push_back((new SlowDown(allCoord["nodes"][i], (typeMesh)12, "SlowDown",10000, 0.1, 0, "third-party/TGUI/widgets/pictures/gift.png")));
             }
            if(typeRandom ==4){
                std::cout << "Type de bonus 4" << std::endl;
                bonus.push_back((new Stop(allCoord["nodes"][i], (typeMesh)12, "Stop",10000, 0.1, 0, "third-party/TGUI/widgets/pictures/gift.png")));
             }

        }

        for(int i=0;i<bonus.size();++i){
            std::cout << glm::to_string(bonus[i]->coord) << std::endl;
        }

        //arbres
        for(int i=0; i < allCoord["tree"].size(); ++i){
            trees.push_back(new Decors(allCoord["tree"][i], mTree, scale/200));
        }
        //rochers
        for(int i=0; i < allCoord["rock"].size(); ++i){
            rocks.push_back(new Decors(allCoord["rock"][i], mRock, scale/200));
        }


      //  std::cout << trees[0].coord << std::endl;

    }



}

glm::mat4 Map::updateModelView()
{
    glm::mat4 scaleMatrix = glm::scale(glm::mat4(1.0f), glm::vec3(scale));
    glm::mat4 translateMatrix = glm::translate(glm::mat4(1.0f),coord);
    return translateMatrix * scaleMatrix;
}

/* Pour afficher toutes les coordonnées des nodes :
 * const std::string path = "./project/resources/map/map1.map";
 * Map map;
 * map.loadConfig(path);
 * for(std::vector<glm::vec3>::const_iterator itr = map.road.begin(); itr != map.road.end(); ++itr)
 * {
 *      std::cout << "x: " << (*itr).x;
 *      std::cout << " y: " << (*itr).y;
 *      std::cout << " z: " << (*itr).z << std::endl;
 * }
 */

void Map::setPathFileMap(std::string map)
{
    if(map == "Polymorphism circuit")
        this->pathFileMap = "./project/resources/map/map1.map";
    else if(map == "Polygon circuit")
        this->pathFileMap = "./project/resources/map/map2.map";
    else if(map == "Polynomial circuit")
        this->pathFileMap = "./project/resources/map/map3.map";
}

void Map::setMesh(std::string type)
{
    if(type == "Country atmosphere")
        this->tMesh = mGrassWorld;
    else if(type == "Desert atmosphere")
        this->tMesh = mSandWorld;
    else if(type == "Mountain atmosphere")
        this->tMesh = mSnowWorld;
}
