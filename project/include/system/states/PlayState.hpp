/*! \file PlayState.hpp
    \brief Etat de jeu qui implémente le monde 3D du jeu.
    \author Audrey Guénée, Adrien Megueddem
    \copyright Imakart
    \language C++
    \updated 2013-11-24
*/

#pragma once

#include "system/states/GameState.hpp"

#include "system/Game.hpp"

#include <stdexcept>

#include <SFML/Graphics.hpp>

#include <TGUI/TGUI.hpp>

#include <iostream>
#include <sstream>

#define THEME_CONFIG_FILE "third-party/TGUI/widgets/Black.conf"

#include "graphics/render/Scene.hpp"

class GameStateManager;

class GameWorld;

/*! \struct PlayState
    structure de l'état de jeu du jeu.
    singleton pour qu'un état ne soit créé qu'une seule fois et reste unique.
 */
class PlayState : public GameState
{
public:
    virtual void enter();
    virtual void leave();

    virtual void pause();
    virtual void resume();

    virtual void events(GameStateManager& gameStateManager, sf::RenderWindow &window);
    virtual void update(GameStateManager& gameStateManager, sf::RenderWindow &window);
    virtual void draw(sf::RenderWindow &window, int fps);

    static PlayState* instance(sf::RenderWindow& window, GameWorld& model)
    {
        static PlayState play(window, model);
        return &play;
    }
protected:
    PlayState(sf::RenderWindow& window, GameWorld& model) :
        gui(window), model(model),
        last_x(0), last_y(0),
        labelFPS(gui), labelVitesse(gui), labelPos(gui), labelCountDown(gui), pictureBonus(gui), labelBonus(gui), labelLap(gui)
    {
        // Initialisation GUI
        if(gui.setGlobalFont("third-party/TGUI/fonts/NexaBold.otf") == false) throw std::runtime_error("PlayState::Failure to open ttf with TGUI");
        // Load all the widgets from a file
        gui.loadWidgetsFromFile("project/src/graphics/gui/play.txt");

        // Initialisation de la flèche de direction
        if (!t_arrow.loadFromFile("project/resources/textures/arrow.png"))
        {
            throw std::runtime_error("PlayState.hpp::PlayState()::Failed to load texture for arrow of direction");
        }
        arrow.setTexture(t_arrow);
        arrow.setColor(sf::Color(0, 255, 0, 128)); // vert

    }
    GameWorld& model;

    Scene scene;

    sf::Texture t_arrow;
    sf::Sprite arrow;

    tgui::Gui gui;
    tgui::Label::Ptr label;
    tgui::Label::Ptr labelFPS;
    tgui::Label::Ptr labelVitesse;
    tgui::Label::Ptr labelPos;
    tgui::Label::Ptr labelCountDown;
    tgui::Picture::Ptr pictureBonus;
    tgui::Label::Ptr labelBonus;
    tgui::Label::Ptr labelLap;

    Timer countDownPlay;

    int countDown;

    int last_x;
    int last_y;
};
