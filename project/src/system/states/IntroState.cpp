#include "system/states/IntroState.hpp"

#include "system/GameStateManager.hpp"
#include "system/states/MenuState.hpp"

#include <stdio.h>
#include <iostream>

#include <SFML/Graphics.hpp>

#include "model/GameWorld.hpp"


void IntroState::enter()
{
    printf("enter : IntroState\n");
    timer.start();
    pilots.push_back(new Pilot);
    pilots[0]->kart.initKart(mDelorean);
    model.rallye.play();
}

void IntroState::leave()
{
    printf("leave : IntroState\n");
    timer.stop();
}

void IntroState::pause()
{
    printf("pause : IntroState\n");
}

void IntroState::resume()
{
    printf("resume : IntroState\n");
}

void IntroState::events(GameStateManager& gameStateManager, sf::RenderWindow &window)
{
    sf::Event e;
    while(window.pollEvent(e)) {
        // évènement "fermeture demandée" : on ferme la fenêtre
        if (e.type == sf::Event::Closed)
            window.close();

        switch(e.type) {
            default:
                break;
            case sf::Event::Closed:
                window.close();
                break;

            case sf::Event::KeyPressed:
                switch(e.key.code){
                    case sf::Keyboard::Space:
                        gameStateManager.changeState(MenuState::instance(window, model));
                        break;
                }
                break;
        }
        gui.handleEvent(e);

        if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
        {
            model.click.play();
        }
    }

    // TGUI callback loop
    tgui::Callback callback;
    while (gui.pollCallback(callback))
    {
        // Make sure tha callback comes from the button
        if(callback.id == 1)
        {
            model.rallye.stop();
            gameStateManager.changeState(MenuState::instance(window, model));
        }
    }
}

void IntroState::update(GameStateManager &gameStateManager, sf::RenderWindow &window)
{
    int startTime = timer.getElapsedTime().asMilliseconds();
    pilots[0]->kart.scale = 0.1f;
    pilots[0]->kart.anim = glm::rotate(glm::mat4(1.0f), 180.f, glm::vec3(0.0, 1.0, 0.0))*glm::translate(glm::mat4(1), glm::vec3(15.f,-2.f,4.f))*glm::translate(glm::mat4(1), glm::vec3(-startTime*0.02f,0.f,0.f));

    // Changement d'état à la fin de l'animation (3500 ms)
    if(startTime >= 3300)
        gameStateManager.changeState(MenuState::instance(window, model));
}

void IntroState::draw(sf::RenderWindow &window, int fps)
{
    window.pushGLStates();
    gui.draw();
    window.popGLStates();
    scene.render(model, pilots);
}
