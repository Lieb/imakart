/*! \file Turbo.hpp
    \brief Implémentations du bonus de turbo
    \author Clara Baudry, Alexandre Bordereau, Audrey Guénée, Angéline Guignard, Adrien Megueddem, Xavier Vansteene
    \copyright Imakart
    \language C++
    \updated 2013-11-21
*/

#pragma once
#include "model/Bonus.hpp"

/*! \class Turbo
    classe du bonus turbo à intercepter dans le jeu
 */
class Turbo: public Bonus
{
    glm::mat4 anim; /*!< matrice d'animation */

public:

    Turbo(glm::vec3 coord, typeMesh tMesh, std::string name,  int duration, float scale, int etat, std::string imageGUI);
    ~Turbo();
    void draw();
    virtual void active(Pilot* pilot, Pilot* pilotfront);
};
