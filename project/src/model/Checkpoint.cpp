#include "model/Checkpoint.hpp"

Checkpoint::Checkpoint(glm::vec3 node, float sc)
{
    coord = node;
    tMesh = mCheckpoint;
    scale = sc;
    anim = glm::mat4(1);
}

Checkpoint::~Checkpoint()
{

}

glm::mat4 Checkpoint::updateModelView()
{
    glm::mat4 scaleMatrix = glm::scale(glm::mat4(1.0f), glm::vec3(scale));
    glm::mat4 translateMatrix = glm::translate(glm::mat4(1.0f), coord);
    return translateMatrix * scaleMatrix;
}
