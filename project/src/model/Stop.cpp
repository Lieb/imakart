#include "model/Stop.hpp"
#include "model/Pilot.hpp"

Stop::Stop(glm::vec3 coord, typeMesh tMesh, std::string name,  int duration, float scale, int etat,std::string imageGUI):
    Bonus(coord, tMesh, name, duration, scale, etat, imageGUI)
{


    compteRebour = Timer(sf::milliseconds(duration),false);

    anim = glm::mat4(1);

}

Stop::~Stop()
{

}

void Stop::draw()
{

}

void Stop::active(Pilot* pilot, Pilot* pilotfront)
{
    std::cout << "STOP ON" << std::endl;
    pilotfront->kart.velocity = 0;
    pilot->bonus = nullptr;

}

