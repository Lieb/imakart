/*! \file Kart.hpp
    \brief Implémentations de la voiture.
    \author Clara Baudry, Alexandre Bordereau, Audrey Guénée, Angéline Guignard, Adrien Megueddem, Xavier Vansteene
    \copyright Imakart
    \language C++
    \updated 2013-11-11
*/

#pragma once
#include <string>
#include "graphics/render/Mesh.hpp"
#include "btBulletDynamicsCommon.h"
#include <glm/glm.hpp>

//#include "model/Stopwatch.hpp"

/*! \struct Kart
    structure d'une voiture
 */
struct Kart
{
    typeMesh tMesh;
    float velocity;         /*!< vitesse actuelle d'une voiture */
    float acceleration;     /*!< vitesse max de la voiture */
    int rank;               /*!< position de la voiture */
    int currentLap;         /*!< tour actuel */
    glm::vec3 currentNode;  /*!< checkpoint visé */
    float scale;            /*!< scale de la voiture (mesh) */
    float distanceToCurrentNode;
    glm::mat4 anim;         /*!< matrice d'animation */
    std::string description;

    // physique
    btCollisionShape* shape;    /*!< forme de la boite de physique qui englobe la voiture */
    btTransform myTransform;
    btDefaultMotionState *myMotionState;
    btRigidBody *body_ground;   /*!< type d'objet physique : objet rigide sur lequel va s'appliquer les gravités et collisions */
    btVector3 localInertia;     /*!< inertie de la voiture */
    btScalar mass;              /*!< masse de la voiture */
    btScalar matrix[15];        /*!< matrice de transformation physique */

    Kart();         /*!< constructeur d'une voiture avec paramètre */
    ~Kart();        /*!< destructeur d'une voiture */

    void move();      /*!< augmente la vitesse et fait avancer une voiture */
    glm::vec3 getDirector(glm::vec3 axis);
    bool isOnCheckpoint();  /*!< on teste si le kart est sur le checkpoint visé */
    void initKart(typeMesh tMesh);

    glm::vec3 getPosition();
    void setPosition(glm::vec3 position);
    glm::vec3 getRotation();
    void setRotation(glm::vec3 Rotation);

    glm::mat4 updateModelView();

    void setTypeMeshNext(typeMesh tMesh);
    void setTypeMeshPrev(typeMesh tMesh);
    typeMesh getTypeMesh();
};
