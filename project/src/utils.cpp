#include <fstream>
#include <sstream>
#include <iostream>
#include <map>
#include <set>
#include <string>
#include <stdexcept>
#include <vector>
#include <cstdlib>
#include <math.h>

#include "utils.hpp"

#define PI 3.14159265359

glm::vec3 sphericalCoordinates(unsigned int lon, unsigned int lat, float r)
{

    float lonRad = PI * lon / 180;
    float latRad = PI * lat / 180;

   float x = r * sin(latRad) * cos(lonRad);
   float y = r * sin(latRad) * sin(lonRad);
   float z = r * cos(latRad); // Insertion dans un vecteur
   glm::vec3 sphericalCoordinates(x, y, z);

   return sphericalCoordinates;
}

/* Conversions */
float convertDegreeToRadian(float angle)
{
    return angle = angle * (PI / 180);
}

float convertRadianToDegree(float rad)
{
    return rad = rad * (180 / PI);
}

glm::vec3 convertBtVectorToGLM(btVector3 bt)
{
    return glm::vec3(bt.getX(), bt.getY(), bt.getZ());
}

std::vector<std::string> getAllLines(std::string path)
{
    // Vérification de l'extension .map
    std::string extension = path.substr(path.find(".")+1, path.find("."));
    std::ifstream file(path);

    if(!file /*|| extension != "map"*/)
        throw std::ios::failure("Unable to open filename : wrong path or wrong extension : " +path);

    // Lecture de toutes les lignes
    std::vector<std::string> allLines;
    std::string line;
    while(std::getline(file, line))
        allLines.push_back(line);

    return allLines;
}

bool parser(std::string path)
{
    // Récupération de toutes les lignes
    std::vector<std::string> allLines = getAllLines(path);

    // Vérification de la première ligne @MAP
    if(allLines.empty() || allLines[0] != "@MAP" || allLines[1] != "")
    {
        std::cout << "The file .map is empty or the checking line is wrong." << std::endl;
        return false;
    }

    // Vérification de la ligne nodes
    if(allLines[2] != "nodes")
    {
        std::cout << "The line nodes is wrong." << std::endl;
        return false;
    }

    // Vérification des coordonnées des noeuds
    unsigned int nodeLon, nodeLat;
    unsigned int i = 3;
    while(allLines[i] != "")
    {
        std::istringstream iss(allLines[i]);
        while(iss >> nodeLon >> nodeLat)
        {
            if(nodeLon < 0 || nodeLon > 360)
            {
                std::cout << "Wrong longitude (nodes)." << std::endl;
                return false;
            }
            if(nodeLat < 0 || nodeLat > 180)
            {
                std::cout << "Wrong latitude (nodes)." << std::endl;
                return false;
            }
        }
        if(!iss.eof())
        {
            std::cout << "Extracting error (nodes)." << std::endl;
            return false;
        }
        iss.str("");
        ++i;
    }

    // Vérification de la ligne setting
    if(allLines[i] != "" || allLines[i+1] != "setting")
    {
        std::cout << "The line setting is wrong." << std::endl;
        return false;
    }

    i += 2;

    // Vérification des types de setting
    if(allLines[i].find("+"))
    {
        std::cout << "Type name is wrong (setting)." << std::endl;
        return false;
    }

    i += 1;

    // Vérification des coordonnées des settings
    unsigned int settingLon, settingLat;
    while(allLines[i] != "")
    {
        if(allLines[i].find("+"))
        {
            std::istringstream iss(allLines[i]);
            while(iss >> settingLon >> settingLat)
            {
                if(settingLon < 0 || settingLon > 360)
                {
                    std::cout << "Wrong longitude (setting)." << std::endl;
                    return false;
                }
                if(settingLat < 0 || settingLat > 180)
                {
                    std::cout << "Wrong latitude (setting)." << std::endl;
                    return false;
                }
            }
            if(!iss.eof())
            {
                std::cout << "Extracting error (setting)." << std::endl;
                return false;
            }
            iss.str("");
            ++i;
        }
        else
        {
            ++i;
        }
    }

    // Vérification de la ligne bonus
    if(allLines[i] != "" || allLines[i+1] != "bonus")
    {
        std::cout << "The line bonus is wrong." << std::endl;
        return false;
    }

    i += 2;

    // Vérification des coordonnées des bonus
    unsigned int bonusLon, bonusLat;
    while(allLines[i] != "")
    {
        std::istringstream iss(allLines[i]);
        while(iss >> bonusLon >> bonusLat)
        {
            if(bonusLat < 0 || bonusLat > 360)
            {
                std::cout << "Wrong longitude (bonus)." << std::endl;
                return false;
            }
            if(bonusLat < 0 || bonusLat > 180)
            {
                std::cout << "Wrong latitude (bonus)." << std::endl;
                return false;
            }
        }
        if(!iss.eof())
        {
            std::cout << "Extracting error (bonus)." << std::endl;
            return false;
        }
        iss.str("");
        ++i;
    }

    // Vérification de la dernière ligne
    if(allLines[i] != "" || allLines[i+1] != "@END")
    {
        std::cout << "The last line is wrong." << std::endl;
        return false;
    }

    return true;
}

mapCoordinates getSphericalCoordinates(std::string path, float r)
{
    mapCoordinates coordinates;

    // Récupération de toutes les lignes
    std::vector<std::string> allLines = getAllLines(path);

    unsigned int i = 0;
    while(allLines[i] != "nodes")
        ++i;

    // Nodes
    std::string nodes = allLines[i];
    ++i;

    std::vector<glm::vec3> nodesCoordinates;
    unsigned int nodeLon, nodeLat;
    while(allLines[i] != "")
    {
        std::istringstream iss(allLines[i]);
        while(iss >> nodeLon >> nodeLat)
        {
            nodesCoordinates.push_back(sphericalCoordinates(nodeLon, nodeLat, r));
        }
        iss.str("");
        ++i;
    }
    coordinates[nodes] = nodesCoordinates;

    i+=2;

    // Setting
    std::string setting = allLines[i].substr(1, allLines[i].find(" "));
    ++i;

    std::vector<glm::vec3> settingCoordinates;
    unsigned int settingLon, settingLat;
    while(allLines[i] != "")
    {
        if(allLines[i].find("+"))
        {
            std::istringstream iss(allLines[i]);
            while(iss >> settingLon >> settingLat)
            {
                settingCoordinates.push_back(sphericalCoordinates(settingLon, settingLat, r));
            }
            iss.str("");
            ++i;
        }
        else
        {
            coordinates[setting] = settingCoordinates;
            settingCoordinates.clear();
            setting = allLines[i].substr(1, allLines[i].find(" "));
            ++i;
        }
    }
    coordinates[setting] = settingCoordinates;

    i+=1;

    // Bonus
    std::string bonus = allLines[i];
    ++i;

    std::vector<glm::vec3> bonusCoordinates;
    unsigned int bonusLon, bonusLat;
    while(allLines[i] != "")
    {
        std::istringstream iss(allLines[i]);
        while(iss >> bonusLon >> bonusLat)
        {
            bonusCoordinates.push_back(sphericalCoordinates(bonusLon, bonusLat, r));
        }
        iss.str("");
        ++i;
    }
    coordinates[bonus] = bonusCoordinates;

    return coordinates;
}

