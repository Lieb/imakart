#include "model/SlowDown.hpp"
#include "model/Pilot.hpp"

SlowDown::SlowDown(glm::vec3 coord, typeMesh tMesh, std::string name,  int duration, float scale, int etat, std::string imageGUI):
    Bonus(coord, tMesh, name, duration, scale,etat,imageGUI)
{

    compteRebour = Timer(sf::milliseconds(duration),false);

    anim = glm::mat4(1);

}

SlowDown::~SlowDown()
{

}

void SlowDown::draw()
{

}

void SlowDown::active(Pilot* pilot, Pilot* pilotfront)
{
    pilot->kart.velocity /=2;
    std::cout << "SLOW DOWN" << std::endl;
    pilot->bonus = nullptr;
}


