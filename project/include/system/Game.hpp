/*! \file Game.hpp
    \brief Coeur du jeu, qui gère la boucle principale.
           Contrôleur en communication avec la vue (graphics) et le modèle (data).
    \author Audrey Guénée, Adrien Megueddem
    \copyright Imakart
    \language C++
    \updated 2013-11-24
*/

#pragma once

#include <SFML/Graphics.hpp>

#include <GL/glew.h>

#include "system/GameStateManager.hpp"
#include "model/Stopwatch.hpp"

#include "model/GameWorld.hpp"

/*! \struct Game
    structure du jeu
 */
class Game
{
public:
    GameWorld model;

    //Déclaration du timer
    StopWatch timer;


    Game();

    int init();
    void run();


private:
    const unsigned int FPS;
    const unsigned int WINDOW_WIDTH;
    const unsigned int WINDOW_HEIGHT;
    const unsigned int FRAMERATE_MILLISECONDS;
    sf::RenderWindow window;
    GLenum glewCode;
    GameStateManager statesManager;

};
