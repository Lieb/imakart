#pragma once


#include "model/Stopwatch.hpp"
#include <SFML/System/Time.hpp>
#include "model/Stopwatch.hpp"

class Timer
{
    public:
        /**
        * @brief Default Constructor. To use the Timer you will need to use restart(sf::Time timeLimit, bool continueRunning).
        */
        Timer();

        /**
        * @brief Recommended constructor.
        * @param timeLimit The starting time of the Timer. It will decrease to zero.
        * @param initRunning Set it true if you want to start the timer at construction
        */
        Timer(sf::Time timeLimit, bool initRunning = false);

        /**
        * @brief Default destructor.
        */
        virtual ~Timer();

        /**
        * @brief Return the time remaining to reach zero, or zero if the reamining timer is over
        * @return The remaining time, in seconds, or zero if timer is expired
        */
        sf::Time    getRemainingTime()      const;

        /**
        * @brief Return if the Timer is running or not
        * @return true if running, else false
        */
        bool    isRunning()             const;

        /**
        * @brief Return if the Timer is expired or not
        * @return true if expired, else false
        */
        bool    isExpired()             const;

        /**
        * @brief To start the Timer
        */
        void    start();

        /**
        * @brief To stop the Timer
        */
        void    stop();

        /**
        * @brief To reset the Timer
        * @param timeLimit The starting time of the Timer. It will decrease to zero.
        * @param continueRunning Set it true if you want to restart the timer
        */
        void    restart(sf::Time timeLimit, bool continueRunning = false);

        /**
        * @brief To reset the Timer, the old time limit will be kept
        * @param continueRunning Set it true if you want to restart the timer
        */
        void    restart(bool continueRunning = false);

    private:
        StopWatch   _clock;
        sf::Time    _timeLimit;
};
