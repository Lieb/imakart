#include "system/states/PlayState.hpp"

#include "system/GameStateManager.hpp"
#include "system/states/PlayMenuState.hpp"
#include "system/states/WinState.hpp"
#include "system/states/LooseState.hpp"

#include <stdio.h>
#include <iostream>
#include <cmath>

#include <glm/gtc/type_ptr.hpp>
#include <glm/ext.hpp>

#include <SFML/Graphics.hpp>

#include "utils.hpp"

#include "graphics/render/Scene.hpp"

#include "model/GameWorld.hpp"
#include "model/Ai.hpp"

#define EPSILON 0.000001


void PlayState::enter()
{
    printf("enter : PlayState\n");
    model.grass.setVolume(20);
    model.grass.play();
    model.grass.setLoop(true);
    countDown = 1;
    countDownPlay = Timer(sf::milliseconds(4000),false);
    countDownPlay.start();


}

void PlayState::leave()
{
    printf("leave : PlayState\n");
    model.grass.stop();
    model.kart.stop();
}

void PlayState::pause()
{
    printf("pause : PlayState\n");
    model.grass.pause();
}

void PlayState::resume()
{
    printf("resume : PlayState\n");
    model.grass.setVolume(75);
    model.grass.play();
}

void PlayState::events(GameStateManager &gameStateManager, sf::RenderWindow &window)
{
    // Variables utiles
    int current_x, current_y;
    float delta = 0.5;

    window.setKeyRepeatEnabled(false);

    sf::Event e;
    while(window.pollEvent(e))
    {

        // évènement "fermeture demandée" : on ferme la fenêtre
        if (e.type == sf::Event::Closed)
            window.close();

        switch(e.type)
        {
            default:
                break;
            case sf::Event::Closed:
                window.close();
                break;

            case sf::Event::KeyPressed:
                switch(e.key.code)
                {
                    case sf::Keyboard::F1:
                        model.debugActive = !model.debugActive;
                        model.switchCamera();
                        break;
                    case sf::Keyboard::F2:
                        break;
                    case sf::Keyboard::Up:
                        //
                        break;
                    case sf::Keyboard::Down:
                        //
                        break;
                    case sf::Keyboard::Right:
                        //
                        break;
                    case sf::Keyboard::Left:
                        //
                        break;
                    case sf::Keyboard::Z:
                        break;
                    case sf::Keyboard::S:
                        break;
                    case sf::Keyboard::D:
                        break;
                    case sf::Keyboard::Q:
                        break;
                    case sf::Keyboard::Space:
                        // Do something
                        break;
                    case sf::Keyboard::Escape:
                        gameStateManager.pushState(PlayMenuState::instance(window, model));
                        break;
                    default:
                        break;
                }
                break;

            case sf::Event::MouseButtonPressed:
                if(e.mouseButton.button == sf::Mouse::Right)
                {
                    last_x = e.mouseButton.x;
                    last_y = e.mouseButton.y;
                }
                break;

            case sf::Event::MouseWheelMoved:
                model.camera->changeDistance(e.mouseWheel.delta*0.5);
                break;
        }
        gui.handleEvent(e);

            // gestion du son
            if (e.type == sf::Event::KeyPressed)
            {
                if(e.key.code == sf::Keyboard::Up)
                {
                    if(countDownPlay.isExpired()){
                        countDown = 0;
                        model.kart.play();
                        model.kart.setLoop(true);
                    }
                }
            }

            if (e.type == sf::Event::KeyReleased)
            {
                if (e.key.code == sf::Keyboard::Up)
                {
                }
            }

            if (e.type == sf::Event::KeyPressed)
            {
                if(e.key.code == sf::Keyboard::Space)
                {
                    if(countDownPlay.isExpired()){
                        countDown = 0;
                        model.catchBonus.play();
                        model.kart.setLoop(true);
                    }
                }
            }

            if (e.type == sf::Event::KeyReleased)
            {
                if (e.key.code == sf::Keyboard::Space)
                {
                    model.catchBonus.stop();
                }
            }
        }
        // UP Pressed : La voiture avance
        if(sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
        {
            if(countDownPlay.isExpired()){
                countDown = 0;
                if(model.pilots[0]->kart.velocity < model.pilots[0]->kart.acceleration)
                {
                    model.pilots[0]->kart.velocity += 0.001;
                }
                else if(model.pilots[0]->kart.velocity > model.pilots[0]->kart.acceleration)
                {
                    model.pilots[0]->kart.velocity -= 0.001;
                }
                model.pilots[0]->kart.move();
            }

            // Bruit du kart qui roule
        }
        // DOWN Pressed : La voiture recule
        else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
        {
            if(countDownPlay.isExpired()){
                countDown = 0;
                if(model.pilots[0]->kart.velocity < -(model.pilots[0]->kart.acceleration/2))
                {
                    model.pilots[0]->kart.velocity = -(model.pilots[0]->kart.acceleration/2);
                }
                else if(model.pilots[0]->kart.velocity <= model.pilots[0]->kart.acceleration && model.pilots[0]->kart.velocity > 0)
                {
                    model.pilots[0]->kart.velocity -= 0.002;
                }
                else if(model.pilots[0]->kart.velocity > -(model.pilots[0]->kart.acceleration/2) && model.pilots[0]->kart.velocity <= 0)
                {
                    model.pilots[0]->kart.velocity -= 0.0005;
                }
                model.pilots[0]->kart.move();
            }
        }
        else
        {
            if(countDownPlay.isExpired()){
                countDown = 0;
                if(model.pilots[0]->kart.velocity > 0)
                {
                    model.pilots[0]->kart.velocity -= 0.001;
                }
                else if(model.pilots[0]->kart.velocity < 0)
                {
                    model.pilots[0]->kart.velocity = 0;
                }
                else
                {
                    model.pilots[0]->kart.velocity = 0;
                }
                model.pilots[0]->kart.move();
            }
        }

        // RIGHT Pressed : La voiture tourne
        if(sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
        {
            if(countDownPlay.isExpired()){
                countDown = 0;
                model.pilots[0]->kart.setRotation(glm::vec3(0,-0.08,0));
            }
        }
        else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
        {
            if(countDownPlay.isExpired()){
                countDown = 0;
                model.pilots[0]->kart.setRotation(glm::vec3(0,0.08,0));
            }
        }

        if(sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
        {
            if(countDownPlay.isExpired()){
                countDown = 0;
                std::cout << "Space is pressed = bonus key" << std::endl;
                if(model.pilots[0]->bonus != nullptr){
                     std::cout << "My pilot has a bonus " <<  std::endl;
                     Pilot* pilotfrontMe = model.getPilotFrontMe(model.pilots[0]->kart.rank);

                     model.pilots[0]->bonus->active(model.pilots[0],pilotfrontMe);
                }
                else {
                    std::cout << "My pilot hasn't a bonus " << std::endl;
                }
            }
    }

    // Mouse Right : Rotation camera
    if(sf::Mouse::isButtonPressed(sf::Mouse::Right))
    {
        current_x = sf::Mouse::getPosition(window).x;
        current_y = sf::Mouse::getPosition(window).y;
        int dX = current_x - last_x, dY = current_y - last_y;
        model.camera->rotateLeft(dX);
        model.camera->rotateUp(dY);
        last_x = current_x;
        last_y = current_y;
    }

    // TGUI callback loop
    tgui::Callback callback;
    while (gui.pollCallback(callback))
    {
        // Make sure tha callback comes from the button
        if(callback.id == 1)
        {
                gameStateManager.pushState(PlayMenuState::instance(window, model));
        }
        if(callback.id == 2)
        {
                gameStateManager.changeState(WinState::instance(window, model));
        }
        if(callback.id == 3)
        {
                gameStateManager.changeState(LooseState::instance(window, model));
        }
    }
}

void PlayState::update(GameStateManager &gameStateManager, sf::RenderWindow &window)
{
    // Update de la physique
    model.updateAttractionForces();
    model.updateMatrixPhysicKarts();
    model.updateMatrixPhysicDecors();

    if ( model.dynamicsWorld )
        model.dynamicsWorld->stepSimulation( 0.08 );

    // Update de la camera
    if(!model.debugActive) model.camera->attachCamera(model.pilots[0]->kart);

    // Update des Ai
    if(countDownPlay.isExpired()){
        countDown = 0;
        for(std::vector<Pilot*>::iterator itr = model.pilots.begin()+1; itr !=  model.pilots.end(); ++itr)
        {
            (*itr)->updateMove();
            (*itr)->updateDir();
            (*itr)->useBonus(model.getPilotFrontMe((*itr)->kart.rank));
        }
    }

    // Update position joueur
    for(std::vector<Pilot*>::const_iterator itr = model.pilots.begin(); itr !=  model.pilots.end(); ++itr)
    {
        bool check = (*itr)->kart.isOnCheckpoint();
        if(check)
        {
            std::cout << "checkpoint passed" << std::endl;
            model.updateCurrentNode((*itr)->kart);
        }
    }
    model.updateRank();

    // endGame and change state if game end
    for(std::vector<Pilot*>::const_iterator itr = model.pilots.begin(); itr !=  model.pilots.end(); ++itr)
    {
        int win = model.endGame((*itr)->kart);
        if(win == 0) gameStateManager.changeState(WinState::instance(window, model));
        else if(win == 1) gameStateManager.changeState(LooseState::instance(window, model));
    }

    model.addBonusToPilots();
    std::cout << "apres addbonus" << std::endl;

    // gestion du son
    if(model.pilots[0]->kart.velocity == 0)
    {
        model.kart.stop();
    }
}

void PlayState::draw(sf::RenderWindow &window, int fps)
{
    // Rendu du model
    scene.render(model);

    // Rendu Debug
    if(model.debugActive == true)
    {
        window.pushGLStates();
            gluPerspective( 70.f, float(1024)/768, 0.1f, 100.f);
            glMultMatrixf(glm::value_ptr(model.camera->getViewMatrix()));
            model.dynamicsWorld->debugDrawWorld();
        window.popGLStates();
    }

    // Sprite arrow direction
    window.pushGLStates();
        arrow.setOrigin(150, 150);
        // position
        arrow.setPosition(sf::Vector2f(512, 50)); // position absolue
        // scale
        arrow.setScale(sf::Vector2f(0.3f, 0.3f)); // facteurs d'échelle absolus
        // rotation
        glm::vec3 dir = model.pilots[0]->kart.getDirector(glm::vec3(1.f, 0.f, 0.f));
        glm::vec2 dirck = model.pilots[0]->getDirectionCheckPoint();
        float angle = convertRadianToDegree(atan2(dir.x*dirck.y - dir.z*dirck.x, dir.x*dirck.x - dir.z*dirck.y));
        arrow.setRotation(-angle);
        window.draw(arrow);
    window.popGLStates();

    // Rendu GUI
    window.pushGLStates();
    if(model.debugActive){
        labelFPS->load(THEME_CONFIG_FILE);
        labelFPS->setSize(1024, 100);
        ssFPS.str("");
        ssFPS << fps;
        labelFPS->setText(ssFPS.str() + " FPS");
        labelFPS->setPosition(500, 20);
        labelFPS->setTextColor(sf::Color(0,0,0));
        labelFPS->setTextSize(24);
    }
    else {
        labelFPS->setText("");
    }
        labelVitesse->load(THEME_CONFIG_FILE);
        labelVitesse->setSize(1024, 100);
        ssVitesse.str("");
        ssVitesse << (int)((model.pilots[0]->kart.velocity)*1000);
        labelVitesse->setText("Speed:  " + ssVitesse.str() + " km/h");
        labelVitesse->setPosition(20, 20);
        labelVitesse->setTextColor(sf::Color(0,0,0));
        labelVitesse->setTextSize(24);

        labelPos->load(THEME_CONFIG_FILE);
        ssPos.str("");
        ssPos << model.pilots[0]->kart.rank;
        labelPos->setText("Position:  " + ssPos.str());
        labelPos->setPosition(20, 55);
        labelPos->setTextColor(sf::Color(0,0,0));
        labelPos->setTextSize(24);

        labelLap->load(THEME_CONFIG_FILE);
        ssLap.str("");
        ssLap << model.pilots[0]->kart.currentLap;
        labelLap->setText("Lap : " + ssLap.str());
        labelLap->setPosition(20, 85);
        labelLap->setTextColor(sf::Color(0,0,0));
        labelLap->setTextSize(24);

        if(model.pilots[0]->bonus != nullptr)
        {
            pictureBonus->load("third-party/TGUI/widgets/pictures/gift.png");
            pictureBonus->setPosition(20,110);
            pictureBonus->setSize(50,50);
            labelBonus->load(THEME_CONFIG_FILE);
            labelBonus->setText(""+model.pilots[0]->bonus->name);
            labelBonus->setPosition(90,105);
            labelBonus->setTextColor(sf::Color(255,0,0));
            labelBonus->setTextSize(24);
            pictureBonus->show();
            labelBonus->show();
        }
        else{
            pictureBonus->hide();
            labelBonus->hide();
        }


        if(countDown == 1){
            labelCountDown->load(THEME_CONFIG_FILE);
            labelCountDown->setSize(1024, 100);

            if(countDownPlay.getRemainingTime()>sf::Time(sf::milliseconds(0))){
                labelCountDown->setText("GO!!");
            }
            if(countDownPlay.getRemainingTime()>sf::Time(sf::milliseconds(500))){
                labelCountDown->setText("Really?!");
            }
            if(countDownPlay.getRemainingTime()>sf::Time(sf::milliseconds(1500))){
                labelCountDown->setText("Are you ready?");
            }
            if(countDownPlay.getRemainingTime()>sf::Time(sf::milliseconds(3000))){
                labelCountDown->setText("Be careful " + model.pilots[0]->name + "!");
            }

            int textSize = labelCountDown->getTextSize();
            labelCountDown->setPosition(100-(textSize/2), 300);
            labelCountDown->setTextColor(sf::Color(255,255,255));
            labelCountDown->setTextSize(80);

            labelCountDown->show();
        }
        else if(countDown == 0){

          labelCountDown->hide();

        }
        gui.draw();
    window.popGLStates();


}
