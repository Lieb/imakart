#include <vector>

#include "system/GameStateManager.hpp"

#include "system/states/GameState.hpp"
#include "system/states/IntroState.hpp"
#include "system/states/MenuState.hpp"
#include "system/states/PlayState.hpp"
#include "system/states/EndState.hpp"

GameStateManager::~GameStateManager(){
    // cleanup the all states
    while (!states.empty()) {
        states.back()->leave();
        states.pop_back();
    }
}

void GameStateManager::changeState(GameState* state)
{
    // leave the current state
    if (!states.empty())
    {
        states.back()->leave();
        states.pop_back();
    }

    // enter the new state
    states.push_back(state);
    states.back()->enter();
}

int GameStateManager::getNbStates() const
{
    return states.size();
}

void GameStateManager::pushState(GameState* state)
{
    // pause current state
    if (!states.empty())
    {
        states.back()->pause();
    }

    // store and init the new state
    states.push_back(state);
    states.back()->enter();
}

void GameStateManager::popState()
{
    if(states.size() > 1)
    {
        // cleanup the current state
        if (!states.empty())
        {
            states.back()->leave();
            states.pop_back();
        }

        // resume previous state
        if (!states.empty())
        {
            states.back()->resume();
        }
    }
}

void GameStateManager::events(sf::RenderWindow& window)
{
    if(states.size() > 0)
    {
        states.back()->events(*this, window);
    }
}

void GameStateManager::update(sf::RenderWindow& window)
{
    if(states.size() > 0)
    {
        states.back()->update(*this, window);
    }
}

void GameStateManager::draw(sf::RenderWindow& window, int fps)
{
    if(states.size() > 0)
    {
        states.back()->draw(window, fps);
    }
}
