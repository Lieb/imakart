#include "system/states/InformationsState.hpp"

#include "system/GameStateManager.hpp"
#include "system/states/MapState.hpp"

#include <stdio.h>

#include <SFML/Graphics.hpp>

#include "model/GameWorld.hpp"


void InformationsState::enter()
{
    printf("enter : InformationsState\n");
    model.configs.setVolume(50);
    model.configs.play();
}

void InformationsState::leave()
{
    printf("leave : InformationsState\n");
}

void InformationsState::pause()
{
    printf("pause : InformationsState\n");
}

void InformationsState::resume()
{
    printf("resume : InformationsState\n");
}

void InformationsState::events(GameStateManager &gameStateManager, sf::RenderWindow &window)
{
    sf::Event e;
    while(window.pollEvent(e)) {
        // évènement "fermeture demandée" : on ferme la fenêtre
        if (e.type == sf::Event::Closed)
            window.close();

        switch(e.type) {
            default:
                break;
            case sf::Event::Closed:
                window.close();
                break;

            case sf::Event::KeyPressed:
                switch(e.key.code){
                    case sf::Keyboard::Return:
                        {
                        // Récupération du pseudo du joueur
                        tgui::EditBox::Ptr editBox1 = gui.get("EditBox1");
                        std::string pseudo = editBox1->getText();
                        model.pilots[0]->setName(pseudo);
                        //std::cout << model.pilots[0]->name << std::endl;

                        gameStateManager.changeState(MapState::instance(window, model));
                        }
                        break;
                    default:
                        break;
                }
                break;
        }

        if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
        {
            model.click.play();
        }

        gui.handleEvent(e);


    }

    // TGUI callback loop
    tgui::Callback callback;
    while (gui.pollCallback(callback))
    {
        // Make sure tha callback comes from the button
        if(callback.id == 1)
        {
            //gameStateManager.changeState(PlayState::instance(window, model));
        }
        // Au clic sur "valider"
        if(callback.id == 2)
        {
            // Récupération du pseudo du joueur
            tgui::EditBox::Ptr editBox1 = gui.get("EditBox1");
            std::string pseudo = editBox1->getText();
            model.pilots[0]->setName(pseudo);
            //std::cout << model.pilots[0]->name << std::endl;

            gameStateManager.changeState(MapState::instance(window, model));
        }
    }
}

void InformationsState::update(GameStateManager &gameStateManager, sf::RenderWindow &window)
{
}

void InformationsState::draw(sf::RenderWindow &window, int fps)
{
    window.pushGLStates();
    gui.draw();
    window.popGLStates();
}
