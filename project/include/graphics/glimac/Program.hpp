/*! \file Program.hpp
    \brief Structure Program qui construit un programme GLSL - glimac
    \author Laurent Noël
    \copyright Imakart
    \language C++
    \updated 2013-11-29
*/

#pragma once

#include <GL/glew.h>
#include "Shader.hpp"

namespace glimac {

    /*! \struct Program
        structure du programme GLSL
     */

	class Program {
		public:
			Program(): m_nGLId(glCreateProgram()) {
			}

			~Program() {
				glDeleteProgram(m_nGLId);
			}

			Program(Program&& rvalue): m_nGLId(rvalue.m_nGLId) {
				rvalue.m_nGLId = 0;
			}

			Program& operator =(Program&& rvalue) {
				m_nGLId = rvalue.m_nGLId;
				rvalue.m_nGLId = 0;
				return *this;
			}

			GLuint getGLId() const {
				return m_nGLId;
			}

			void attachShader(const Shader& shader) {
				glAttachShader(m_nGLId, shader.getGLId());
			}

			bool link();

			const std::string getInfoLog() const;

			void use() const {
				glUseProgram(m_nGLId);
			}

		private:
			Program(const Program&);
			Program& operator =(const Program&);

			GLuint m_nGLId;
	};

	// Build a GLSL program from source code
    Program buildProgram(const GLchar* vsSrc, const GLchar* fsSrc); /*!< constructeur d'un programme GLSL depuis le code source */

	// Load source code from files and build a GLSL program
    Program loadProgram(const char* vsFile, const char* fsFile); /*!< chargement du code source depuis les shaders et construction d'un programme GLSL */

}
