#include "system/states/KartState.hpp"

#include "system/GameStateManager.hpp"
#include "system/states/PlayState.hpp"

#include <stdio.h>

#include <SFML/Graphics.hpp>

#include "model/GameWorld.hpp"

#include "model/Pilot.hpp"


void KartState::enter()
{
    printf("enter : KartState\n");
    timer.start();
    pilots.push_back(new Pilot);
    pilots[0]->kart.initKart(mBuggy);
}

void KartState::leave()
{
    printf("leave : KartState\n");
    model.configs.stop();
}

void KartState::pause()
{
    printf("pause : KartState\n");
}

void KartState::resume()
{
    printf("resume : KartState\n");
}

void KartState::events(GameStateManager &gameStateManager, sf::RenderWindow &window)
{
    sf::Event e;
    while(window.pollEvent(e))
    {
        // évènement "fermeture demandée" : on ferme la fenêtre
        if (e.type == sf::Event::Closed)
            window.close();

        switch(e.type)
        {
            default:
                break;
            case sf::Event::Closed:
                window.close();
                break;

            case sf::Event::KeyPressed:
                switch(e.key.code)
                {
                    case sf::Keyboard::Space:
                        // gameStateManager.changeState(PlayState::instance(window));
                        break;
                }
                break;
        }

        if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
        {
            model.click.play();
        }
        gui.handleEvent(e);
    }

    // TGUI callback loop
    tgui::Callback callback;
    while (gui.pollCallback(callback))
    {
        // Make sure tha callback comes from the button
        if(callback.id == 1)
        {

        }
        // Au clic sur la flèche précédente
        if(callback.id == 2)
        {
            // Màj du tMesh du kart sélectionné
            pilots[0]->kart.setTypeMeshPrev(pilots[0]->kart.tMesh);
            // Màj de la description du kart sélectionné
            tgui::TextBox::Ptr textbox = gui.get("TextBox1");
            textbox->setText(pilots[0]->kart.description);
        }
        // Au clic sur la flèche suivante
        if(callback.id == 3)
        {
            // Màj du tMesh du kart sélectionné
            pilots[0]->kart.setTypeMeshNext(pilots[0]->kart.tMesh);
            // Màj de la description du kart sélectionné
            tgui::TextBox::Ptr textbox = gui.get("TextBox1");
            textbox->setText(pilots[0]->kart.description);
        }
        // Au clic sur "valider"
        if(callback.id == 4)
        {
            // Enregistrement du kart choisi dans la structure pilot du model
            model.pilots[0]->kart.tMesh = pilots[0]->kart.getTypeMesh();

            gameStateManager.changeState(PlayState::instance(window, model));
        }
    }
}

void KartState::update(GameStateManager &gameStateManager, sf::RenderWindow &window)
{
    int startTime = timer.getElapsedTime().asMilliseconds();
    pilots[0]->kart.anim = glm::translate(glm::mat4(1), glm::vec3(0.f,0.3f,-1.5f))*glm::rotate(glm::mat4(1.0f), -startTime*0.08f, glm::vec3(0.0, 1.0, 0.0));
}

void KartState::draw(sf::RenderWindow &window, int fps)
{
    window.pushGLStates();
    gui.draw();
    window.popGLStates();
    scene.render(model, pilots);
}
