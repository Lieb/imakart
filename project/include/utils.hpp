/*! \file utils.hpp
    \brief Implémentation de fonctions utilitaires.
    \author Clara Baudry, Alexandre Bordereau, Audrey Guénée, Angéline Guignard, Adrien Megueddem, Xavier Vansteene
    \copyright Imakart
    \language C++
    \updated 2013-12-22
*/

#pragma once

#include <vector>
#include <string>
#include <map>

#include <glm/glm.hpp>
#include "btBulletDynamicsCommon.h"

glm::vec3 sphericalCoordinates(unsigned int lon, unsigned int lat, float r); /*!< convertit la latitude et la longitude en coordonnées sphériques (x, y, z) */
float convertDegreeToRadian(float angle);
float convertRadianToDegree(float rad);
glm::vec3 convertBtVectorToGLM(btVector3 bt);

std::vector<std::string> getAllLines(std::string path); /*!< récupére toutes les lignes d'un fichier .map */
bool parser(std::string path); /*!< vérifie la validité d'un fichier .map */

typedef std::map<std::string, std::vector<glm::vec3>> mapCoordinates; /*!< définit un tableau associatif des coordonnées d'un fichier .map (nodes, setting, bonus) */
mapCoordinates getSphericalCoordinates(std::string path, float r); /*!< récupére toutes les coordonnées d'un fichier .map (nodes, setting, bonus) dans une tableau associatif */
/*
 * Pour accéder aux coordonnées du 1er noeud de la carte (par exemple) :
 * std::cout << coord["nodes"][0].x << std::endl;
 * std::cout << coord["nodes"][0].y << std::endl;
 * std::cout << coord["nodes"][0].z << std::endl;
 *
*/
