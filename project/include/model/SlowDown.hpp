/*! \file SlowDown.hpp
    \brief Implémentations du bonus de ralentissement
    \author Clara Baudry, Alexandre Bordereau, Audrey Guénée, Angéline Guignard, Adrien Megueddem, Xavier Vansteene
    \copyright Imakart
    \language C++
    \updated 2013-11-21
*/

#pragma once
#include "model/Bonus.hpp"

/*! \class SlowDown
    classe du bonus SlowDown à intercepter dans le jeu
 */
class SlowDown: public Bonus
{
    glm::mat4 anim; /*!< matrice d'animation */

public:


    SlowDown(glm::vec3 coord, typeMesh tMesh, std::string name,  int duration, float scale, int etat, std::string imageGUI);
    ~SlowDown();
    void draw();
    virtual void active(Pilot *pilot, Pilot *pilotfront);   /*!< activation du stop à un Pilot */
};
