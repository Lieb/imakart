/*! \file Pilot.hpp
    \brief Implémentations du pilote.
    \author Clara Baudry, Alexandre Bordereau, Audrey Guénée, Angéline Guignard, Adrien Megueddem, Xavier Vansteene
    \copyright Imakart
    \language C++
    \updated 2013-11-07
*/

#pragma once
#include <string>
#include "model/Kart.hpp"
#include "model/Time.hpp"
#include "model/Bonus.hpp"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/*! \class Pilot
    classe du pilote "humain" du jeu
 */
class Pilot
{
public:
    std::string name;
    Bonus* bonus;
    Time time;
    Kart kart;

    Pilot();
    virtual~Pilot();

    void initKart();
    Pilot PilotFront();
    Pilot PilotBack();

    void setName(std::string name);

    bool addBonus(Bonus *b);

    glm::vec2 getDirectionCheckPoint();

    virtual void updateDir();
    virtual void avoid(glm::vec3 object);
    virtual void useBonus(Pilot* frontPilot);
    virtual void updateMove();
};
