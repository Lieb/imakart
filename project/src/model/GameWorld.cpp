#include "model/GameWorld.hpp"
#include "model/Ai.hpp"
#include "model/Decors.hpp"

#include <iostream>
#include <glm/ext.hpp>
#include <algorithm>
#include <stdexcept>

GameWorld::GameWorld()
{
    broadphase = new btDbvtBroadphase();
    collisionConfiguration = new btDefaultCollisionConfiguration();
    dispatcher = new btCollisionDispatcher(collisionConfiguration);
    solver = new btSequentialImpulseConstraintSolver;
    dynamicsWorld = new btDiscreteDynamicsWorld(dispatcher,broadphase,solver,collisionConfiguration);
    dynamicsWorld->setGravity(btVector3(0,0,0));    //gravité du monde physique
    debugDrawer.setDebugMode(btIDebugDraw::DBG_MAX_DEBUG_DRAW_MODE);
    dynamicsWorld->setDebugDrawer(&debugDrawer);
    debugActive = false;
}

GameWorld::~GameWorld()
{
    delete broadphase;
    delete collisionConfiguration;
    delete dispatcher;
    delete solver;
    delete dynamicsWorld;
    //pilots
    for (int i = 0; i < pilots.size(); ++i)
    {
        delete pilots[i];
    }
    pilots.clear();

    //positions
    for (int i = 0; i < positions.size(); ++i)
    {
        delete positions[i];
    }
    positions.clear();

    //checkpoint
    for (int i = 0; i < checkpoint.size(); ++i)
    {
        delete checkpoint[i];
    }
    checkpoint.clear();
}

void GameWorld::initWorld()
{
    // Basic
    initMeshs();
    initSounds();

    // creation des maps
    initMap();

    // creation des pilots
    initPilots();
    initPositions();

    // Initialisation de la camera

    initCamera();

    //on ajoute les objets au dynamic world
    addObjectToDynamicWorld();
}

void GameWorld::initSounds()
{
    if (!configs.openFromFile("./project/resources/sound/Config - Yuna Song.ogg"))
        throw std::runtime_error("State::Failed to load sound : ./project/resources/sound/Config - Yuna Song.ogg");
    if (!menus.openFromFile("./project/resources/sound/Menu - One Step Closer.ogg"))
        throw std::runtime_error("State::Failed to load sound : ./project/resources/sound/Menu - One Step Closer.ogg");
    if (!sand.openFromFile("./project/resources/sound/Play - Desert.ogg"))
        throw std::runtime_error("State::Failed to load sound : ./project/resources/sound/Play - Desert.ogg");
    if (!grass.openFromFile("./project/resources/sound/Play - Gambler of Highway.ogg"))
        throw std::runtime_error("State::Failed to load sound : ./project/resources/sound/Play - Gambler of Highway.ogg");
    if (!credits.openFromFile("./project/resources/sound/Credits - Theme of Al De Baren.ogg"))
        throw std::runtime_error("State::Failed to load sound : ./project/resources/sound/Credits - Theme of Al De Baren.ogg");
    
    // init des bruitages 
    if (!click_b.loadFromFile("./project/resources/sound/clic_souris.ogg"))
        throw std::runtime_error("State::Failed to load sound : ./project/resources/sound/clic_souris.ogg");
    click.setBuffer(click_b);
    if (!catchBonus_b.loadFromFile("./project/resources/sound/recupere_bonus.ogg"))
        throw std::runtime_error("State::Failed to load sound : ./project/resources/sound/recupere_bonus.ogg");
    catchBonus.setBuffer(catchBonus_b);    
    if (!win_b.loadFromFile("./project/resources/sound/gagne.ogg"))
        throw std::runtime_error("State::Failed to load sound : ./project/resources/sound/gagne.ogg");
    win.setBuffer(win_b);  
    if (!loose_b.loadFromFile("./project/resources/sound/perdu.ogg"))
        throw std::runtime_error("State::Failed to load sound : ./project/resources/sound/perdu.ogg");
    loose.setBuffer(loose_b);   
    if (!kart_b.loadFromFile("./project/resources/sound/kart_1.ogg"))
        throw std::runtime_error("State::Failed to load sound : ./project/resources/sound/kart_1.ogg");
    kart.setBuffer(kart_b);   
    if (!rallye_b.loadFromFile("./project/resources/sound/rallye.ogg"))
        throw std::runtime_error("State::Failed to load sound : ./project/resources/sound/kart_1.ogg");
    rallye.setBuffer(rallye_b);

}

void GameWorld::initMeshs()
{
    meshs[mBuggy] = new Mesh("./project/resources/obj/buggy.obj", "./project/resources/textures/buggyTex.png");
    meshs[mF1] = new Mesh("./project/resources/obj/f1.obj", "./project/resources/textures/f1tex.png");
    meshs[mDelorean] = new Mesh("./project/resources/obj/delorean.obj", "./project/resources/textures/deloreanTex.png");
    meshs[mMonsterTruck] = new Mesh("./project/resources/obj/monsterTruck.obj", "./project/resources/textures/test.png");
    meshs[mBonus] = new Mesh("./project/resources/obj/bonus.obj", "./project/resources/textures/bonus.png");
    meshs[mSandWorld] = new Mesh("./project/resources/obj/sphere.obj", "./project/resources/textures/sable.png");
    meshs[mGrassWorld] = new Mesh("./project/resources/obj/sphere.obj", "./project/resources/textures/herbe.png");
    meshs[mSnowWorld] = new Mesh("./project/resources/obj/sphere.obj", "./project/resources/textures/neige.png");
    meshs[mSkybox] = new Mesh("./project/resources/obj/sphere.obj", "./project/resources/textures/sky.png");
    meshs[mTree] = new Mesh("./project/resources/obj/tree.obj", "./project/resources/textures/buisson.png");
    meshs[mFlower] = new Mesh("./project/resources/obj/flower.obj", "./project/resources/textures/orange.png");
    //meshs[mFlower] = new Mesh("./project/resources/obj/flower.obj", "./project/resources/textures/white.png");
    meshs[mRock] = new Mesh("./project/resources/obj/rock.obj", "./project/resources/textures/rock.png");
    meshs[mCheckpoint] = new Mesh("./project/resources/obj/checkpoint.obj","./project/resources/textures/checkpoint.png");
}

void GameWorld::initMap()
{
    map.loadConfig("./project/resources/map/map1.map");
/*
    for(std::vector<glm::vec3>::const_iterator itr = map.road.begin(); itr != map.road.end(); ++itr)
      {
           std::cout << "x: " << (*itr).x;
           std::cout << " y: " << (*itr).y;
           std::cout << " z: " << (*itr).z << std::endl;
      }
*/
   // meshs[mRoad] = new Mesh(map.road);
}

void GameWorld::initPilots()
{
    pilots.push_back(new Pilot);
    pilots.push_back(new Ai);
    pilots.push_back(new Ai);
//    pilots.push_back(new Ai);

    // Add current checkPoint
    for(std::vector<Pilot*>::iterator itr = pilots.begin(); itr != pilots.end(); ++itr)
    {
        (*itr)->kart.currentNode = map.checkpoints[1].coord;
    }

    // Add position
    int placement = pilots.size()/2;
    for(std::vector<Pilot*>::iterator itr = pilots.begin(); itr != pilots.end(); ++itr)
    {
        // Calcule en dur directement sur les positions pour placer les karts sur la ligne de départ les unes à coté des autres.
        (*itr)->kart.setPosition(glm::vec3(map.checkpoints[0].coord.x-1.5, map.checkpoints[0].coord.y, map.checkpoints[0].coord.z-placement));
        ++placement;
    }
}

void GameWorld::initPositions()
{
    for(std::vector<Pilot*>::iterator itr = pilots.begin(); itr != pilots.end(); ++itr)
    {
        positions.push_back(*itr);
    }
}

void GameWorld::initCamera()
{
    camera1 = TrackballCamera(map.trees[0]->getPosition(), 1.f, 0.f, 0.f);
    camera = &camera2;
}

void GameWorld::updateRank()
{
    std::cout <<"checkpoint : " << glm::to_string(pilots[1]->kart.currentNode) << std::endl;
    std::cout << "num : " << getIndexCheckpoint(pilots[1]->kart.currentNode)<< std::endl;

    bool swap = false;
    do
    {
        for(std::vector<Pilot*>::iterator itr = positions.begin(); itr != positions.end()-1; ++itr)
        {
            std::vector<Pilot*>::iterator next_itr = itr;
            ++next_itr;
            // meme tour
            if((*itr)->kart.currentLap == (*next_itr)->kart.currentLap)
            {
                int indexCheckpoint_itr = getIndexCheckpoint((*itr)->kart.currentNode);
                int indexCheckpoint_next_itr = getIndexCheckpoint((*next_itr)->kart.currentNode);
                // meme checkpoint
                if(indexCheckpoint_itr == indexCheckpoint_next_itr)
                {
                    if((*itr)->kart.distanceToCurrentNode > (*next_itr)->kart.distanceToCurrentNode)
                    {
                        std::cout << "distance kart 1 : " << (*itr)->kart.distanceToCurrentNode << std::endl;
                        std::iter_swap(itr, next_itr);
                        swap = true;
                    }
                    else{
                        std::cout << "distance kart 1 : " << (*itr)->kart.distanceToCurrentNode << std::endl;
                        swap = false;
                    }
                }
                else if(indexCheckpoint_itr < indexCheckpoint_next_itr)
                {
                    std::cout << "distance kart 1 : " << (*itr)->kart.distanceToCurrentNode << std::endl;
                    std::iter_swap(itr, next_itr);
                    swap = true;
                }
                else{
                    std::cout << "distance kart 1 : " << (*itr)->kart.distanceToCurrentNode << std::endl;
                    swap = false;
                }
            }
            else if((*itr)->kart.currentLap < (*next_itr)->kart.currentLap)
            {
                std::iter_swap(itr, next_itr);
                swap = true;
            }
            else{
                swap = false;
            }
        }

    }while(swap);
    int i = 1;
    for(std::vector<Pilot*>::iterator itr = positions.begin(); itr != positions.end(); ++itr)
    {
        (*itr)->kart.rank = i;
        ++i;
    }
}

int GameWorld::getIndexCheckpoint(glm::vec3 coord)
{
    int i = 0;
    for(std::vector<Checkpoint>::iterator itr = map.checkpoints.begin(); itr != map.checkpoints.end(); ++itr)
    {
        if(itr->coord == coord)
        {
            return i;
        }
        ++i;
    }
    throw std::range_error("Gameworld::getIndexCheckpoint aucun checkpoint correspondant");
}

void GameWorld::updateCurrentNode(Kart &kart)
{
    int indexOldNode = getIndexCheckpoint(kart.currentNode);
    std::cout << "index courant " << indexOldNode <<std::endl;
    if(indexOldNode < map.checkpoints.size()-1)
    {
        std::cout << "index != size" <<std::endl;
       ++indexOldNode;
        std::cout << "index new = " << indexOldNode<<std::endl;
       kart.currentNode = map.checkpoints[indexOldNode].coord;
    }
    else{
        indexOldNode = 0;
        kart.currentLap++;
    }
}

int GameWorld::endGame(Kart &kart)
{
    std::cout << "current lap : " << kart.currentLap <<std::endl;
    if(kart.currentLap == 30)
    {
        // si le joueur humain a terminé premier
        if(pilots[0] == positions[0])
        {
            // Clean Model à appeller ici
            return 0;
        }
        else{
            // Clean Model à appeller ici
            return 1;
        }
    }
    return 2;
}

void GameWorld::switchCamera()
{
    if(camera == &camera1)
    {
        camera = &camera2;
    }
    else if(camera == &camera2)
    {
        camera = &camera1;
    }
}

/* Physic */
void GameWorld::updateAttractionForces()
{
    for(std::vector<Pilot*>::const_iterator itr = pilots.begin(); itr !=  pilots.end(); ++itr)
    {
        btVector3 force;
        force = map.body_ground->getCenterOfMassPosition() - (*itr)->kart.body_ground->getCenterOfMassPosition();
        force.normalize();
        force *= 9.81f; //gravité terrestre
        force *= 20000; //masse de la planete
        (*itr)->kart.body_ground->applyCentralForce(force * 0.001); //application de la force
    }
}

void GameWorld::addObjectToDynamicWorld()
{
    // map
    dynamicsWorld->addRigidBody(map.body_ground);

    // karts
    for(std::vector<Pilot*>::const_iterator itr = pilots.begin(); itr !=  pilots.end(); ++itr)
    {
        dynamicsWorld->addRigidBody((*itr)->kart.body_ground);
    }

    // décors
    for(std::vector<Decors*>::const_iterator itr = map.trees.begin(); itr !=  map.trees.end(); ++itr)
    {
        dynamicsWorld->addRigidBody((*itr)->body_ground);
    }

    for(std::vector<Decors*>::const_iterator itr = map.rocks.begin(); itr !=  map.rocks.end(); ++itr)
    {
        dynamicsWorld->addRigidBody((*itr)->body_ground);
    }
}

void GameWorld::updateMatrixPhysicKarts()
{
    for(std::vector<Pilot*>::const_iterator itr = pilots.begin(); itr !=  pilots.end(); ++itr)
    {
        (*itr)->kart.body_ground->activate(true);
        //(*itr)->kart.myMotionState->m_graphicsWorldTrans.setFromOpenGLMatrix(glm::value_ptr((*itr)->kart.coord)); // A voir si il ne faut pas set le MotionState aussi et pas seulement le récupérer.
        (*itr)->kart.myMotionState->m_graphicsWorldTrans.getOpenGLMatrix((*itr)->kart.matrix);
    }
}

void GameWorld::updateMatrixPhysicDecors()
{
    for(std::vector<Decors*>::iterator itr = map.trees.begin(); itr !=  map.trees.end(); ++itr)
    {
        (*itr)->myMotionState->m_graphicsWorldTrans.getOpenGLMatrix((*itr)->matrix);
    }
    for(std::vector<Decors*>::iterator itr = map.rocks.begin(); itr !=  map.rocks.end(); ++itr)
    {
        (*itr)->myMotionState->m_graphicsWorldTrans.getOpenGLMatrix((*itr)->matrix);
    }
}

Pilot* GameWorld::getPilotFrontMe(int pilotPos){
    if(pilotPos == 1)
        return positions[pilotPos];
    else
        return positions[pilotPos-2];
}

void GameWorld::addBonusToPilots(){

    for(std::vector<Pilot*>::const_iterator itr = pilots.begin(); itr !=  pilots.end(); ++itr){

        glm::vec3 posPilot = (*itr)->kart.getPosition(); // je récupère la position glm::vec3 du pilote j

        for(std::vector<Bonus*>::iterator itrBonus = map.bonus.begin(); itrBonus !=  map.bonus.end(); ++itrBonus){
            glm::vec3 posBonus = (*itrBonus)->coord; // je récupère la position glm::vec3 du bonus i
            //Je calcule la distance entre les deux
            float distance = sqrt(pow(posBonus.x - posPilot.x,2) + pow(posBonus.y - posPilot.y,2) + pow(posBonus.z - posPilot.z,2));
            if(distance<0.25){
                //Le pilote j est sur le bonus i
                std::cout << "PILOT ON BONUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUS" << std::endl;

                //pointeur bonus dans pilote = pointeur du bonus i -> équivalent à la fonction addBonus()
                (*itr)->bonus = (*itrBonus);
                std::cout << pilots[0]->bonus << std::endl;

                //EGALITE DES ADRESSES OK = verification de l'égalité des deux pointeurs
                std::cout << "map bonus : " <<(*itrBonus) << std::endl;
                std::cout << "ptr bonus pilot game : " << (*itr)->bonus << std::endl;

                //Si le pointeur bonus de pilot n'est pas nul, alors
                if((*itr)->bonus !=nullptr)
                {
                    //On accéde au pointeur de bonus dans pilote sans problème ici
                    std::cout << (*itr)->bonus->name << std::endl; // Affichage du "name" du bonus
                    //Pilot* pilotfrontMe = getPilotFrontMe((*itr)->kart.rank); //On récupère un pointeur sur le pilot devant le pilot j ou on retourne j si il est le premier
                    //(*itr)->bonus->active((*itr),pilotfrontMe); //Lancement de l'effet du bonus
                }
            }
         }
    }

}
