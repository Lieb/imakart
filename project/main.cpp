#include <cstdlib>
#include "system/Game.hpp"

#include "model/Kart.hpp"
#include <vector>

//static const unsigned int FPS = 30;

//static const unsigned int WINDOW_WIDTH = 800;
//static const unsigned int WINDOW_HEIGHT = 600;

int main(int argc, char** argv)
{
    Game imakart;
    imakart.init();
    imakart.run();

    return (EXIT_SUCCESS);
}
