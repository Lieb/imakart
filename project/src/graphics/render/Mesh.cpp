#include "graphics/render/Mesh.hpp"

#include <stdexcept>

#include <SFML/Graphics.hpp>

#include <glm/glm.hpp>
#include <glm/ext.hpp>

#include <iostream>


Mesh::Mesh(std::string pathObj, std::string pathTexture)
{
    glGenBuffers(1, &vbo);
    glGenVertexArrays(1, &vao);

    // loadObj
    if(!loadObj(pathObj)) throw std::runtime_error("Mesh.cpp::Failed to load .obj : " + pathObj);




    // VBO
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(glimac::ShapeVertex)*vertices.size(), &vertices[0], GL_STATIC_DRAW); // /!\ Pour retourner un pointeur avec un vector il faut pointer sur l'élément [0]puis renvoyer l'adresse.
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // VAO
    glBindVertexArray(vao);
        glEnableVertexAttribArray(0); // activation des attributs de vertexs
        glEnableVertexAttribArray(1); // activation des attributs de normales
        glEnableVertexAttribArray(2); // activation des attributs de textures
            glBindBuffer(GL_ARRAY_BUFFER, vbo);
                glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(glimac::ShapeVertex), (const GLvoid*) offsetof(glimac::ShapeVertex,position));
                glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(glimac::ShapeVertex), (const GLvoid*) offsetof(glimac::ShapeVertex,normal));
                glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(glimac::ShapeVertex), (const GLvoid*) offsetof(glimac::ShapeVertex,texCoords));
            glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);


    // Texture
    sf::Image img_data;
    if (!img_data.loadFromFile(pathTexture))
    {
        throw std::runtime_error("Could not load : "+ pathTexture);
    }

    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
        glTexImage2D(
            GL_TEXTURE_2D,
            0,
            GL_RGBA,
            img_data.getSize().x,
            img_data.getSize().y,
            0,
            GL_RGBA,
            GL_UNSIGNED_BYTE,
            img_data.getPixelsPtr()
        );
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glBindTexture(GL_TEXTURE_2D, 0);

}


Mesh::Mesh(std::vector<glm::vec3> nodes){
    glGenBuffers(1, &vbo);
    glGenVertexArrays(1, &vao);

    // loadObj
    if(!loadNodes(nodes)) throw std::runtime_error("Mesh.cpp::Failed to load map");




    // VBO
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(glimac::ShapeVertex)*vertices.size(), &vertices[0], GL_STATIC_DRAW); // /!\ Pour retourner un pointeur avec un vector il faut pointer sur l'élément [0]puis renvoyer l'adresse.
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // VAO
    glBindVertexArray(vao);
        glEnableVertexAttribArray(0); // activation des attributs de vertexs
        glEnableVertexAttribArray(1); // activation des attributs de normales
        glEnableVertexAttribArray(2); // activation des attributs de textures
            glBindBuffer(GL_ARRAY_BUFFER, vbo);
                glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(glimac::ShapeVertex), (const GLvoid*) offsetof(glimac::ShapeVertex,position));
                glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(glimac::ShapeVertex), (const GLvoid*) offsetof(glimac::ShapeVertex,normal));
                glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(glimac::ShapeVertex), (const GLvoid*) offsetof(glimac::ShapeVertex,texCoords));
            glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

}


Mesh::~Mesh()
{
    glDeleteBuffers(1, &vbo);
    glDeleteVertexArrays(1, &vao);
    glDeleteTextures(1,  &texture);
}


bool Mesh::loadObj(const std::string path)
{
    Assimp::Importer importer;
    const aiScene* scene = importer.ReadFile( path, aiProcess_CalcTangentSpace | aiProcess_Triangulate | aiProcess_SortByPType);

    if (!scene) {
        std::cerr << "Mesh.cpp::error: reading object file \n" << std::endl;
        return false;
    }

    for (unsigned int m_i = 0; m_i < scene->mNumMeshes; ++m_i) {
        const aiMesh* mesh = scene->mMeshes[m_i];

        if(!mesh->HasPositions() || !mesh->HasNormals() || !mesh->HasTextureCoords(0)) throw std::runtime_error("Mesh.cpp::loadObj()::.obj is not correct." + path);
        for (unsigned int v_i = 0; v_i < mesh->mNumVertices; ++v_i) {
            glimac::ShapeVertex tmp;
            const aiVector3D* vp = &(mesh->mVertices[v_i]);
            tmp.position = glm::vec3(vp->x, vp->y, vp->z);
            const aiVector3D* vn = &(mesh->mNormals[v_i]);
            tmp.normal = glm::vec3(vn->x, vn->y, vn->z);
            const aiVector3D* vt = &(mesh->mTextureCoords[0][v_i]);
            tmp.texCoords = glm::vec2(vt->x, vt->y);
            // Remplissage vertices.
            vertices.push_back(tmp);
        }
    }
    return true;
}

bool Mesh::loadNodes(std::vector<glm::vec3> Nodes)
{
    for (std::vector<glm::vec3>::const_iterator itr = Nodes.begin(); itr != Nodes.end(); ++itr) {
        glimac::ShapeVertex tmp;
        tmp.position = (*itr);
        tmp.normal = glm::vec3(0);
        tmp.texCoords = glm::vec2(0);
        vertices.push_back(tmp);
    }
        return true;
}


void Mesh::render(GLuint prog, glm::mat4 mvMatrix, GLenum typeDraw)
{
    /// Uniformes ///
    GLint locationUniformMVPMatrix = glGetUniformLocation(prog, "uMVPMatrix");
    GLint locationUniformMVMatrix = glGetUniformLocation(prog, "uMVMatrix");
    GLint locationUniformNormalMatrix = glGetUniformLocation(prog, "uNormalMatrix");
    //GLint locationUniformTextureMatrix = glGetUniformLocation(myprog.getGLId(), "uTextureMatrix");

    //lumiere
    GLint uKd = glGetUniformLocation(prog, "uKd");
    GLint uKs = glGetUniformLocation(prog, "uKs");
    GLint uShininess = glGetUniformLocation(prog, "uShininess");

    GLint uLightDir_vs = glGetUniformLocation(prog, "uLightDir_vs");
    GLint uLightIntensity = glGetUniformLocation(prog, "uLightIntensity");

    glm::mat4 ProjMatrix;
    glm::mat4 MVMatrix;
    glm::mat4 NormalMatrix;
    //glm::vec3 TextureVector;

    ProjMatrix = glm::perspective(70.f, float(1024)/768, 0.1f, 100.f);
    MVMatrix = mvMatrix;
    NormalMatrix = glm::transpose(glm::inverse(MVMatrix));

    //lumiere
    glUniform3f(uKd, 1, 1, 1);
    glUniform3f(uKs, 0, 0, 0);
    glUniform1f(uShininess, 16);

    glUniform3f(uLightIntensity, 0.8, 0.8, 0.8);

    glUniform3fv(uLightDir_vs, 1, glm::value_ptr(glm::vec3(1)));


    // Envoie des variables uniformess
    glUniformMatrix4fv(locationUniformMVPMatrix, 1, false, glm::value_ptr(ProjMatrix*MVMatrix));
    glUniformMatrix4fv(locationUniformMVMatrix, 1, false, glm::value_ptr(MVMatrix));
    glUniformMatrix4fv(locationUniformNormalMatrix, 1, false, glm::value_ptr(NormalMatrix));

    // Dessins des données dans les tableau après bind du vao.
    glBindVertexArray(vao);
        glBindTexture(GL_TEXTURE_2D, texture);
            glDrawArrays(typeDraw, 0, vertices.size());
        glBindTexture(GL_TEXTURE_2D, 0);
    glBindVertexArray(0);
}
