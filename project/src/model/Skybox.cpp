#include "model/Skybox.hpp"

#include "system/Game.hpp"

Skybox::Skybox()
{
    tMesh = mSkybox;
    scale = 95.f;
    anim = glm::mat4(1);

    rotationTimer.start();
}

Skybox::~Skybox()
{

}

glm::mat4 Skybox::updateModelView()
{
    int startTime = rotationTimer.getElapsedTime().asMilliseconds();
    glm::mat4 rotateMatrix = glm::rotate(glm::mat4(1.0f), startTime*0.004f, glm::vec3(0.0, 1.0, 0.0));
    glm::mat4 scaleMatrix = glm::scale(glm::mat4(1.0f), glm::vec3(scale));
    glm::mat4 translateMatrix = glm::translate(glm::mat4(1.0f), coord);
    return translateMatrix * scaleMatrix * rotateMatrix;
}
