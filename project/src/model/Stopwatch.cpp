#include "model/Stopwatch.hpp"

StopWatch::StopWatch(bool initrunning) : _buffer(sf::Time::Zero), _running(initrunning)
{
    //ctor
}

StopWatch::~StopWatch()
{
    //dtor
}

sf::Time StopWatch::getElapsedTime() const
{
    if(_running)
        return (_buffer + _clock.getElapsedTime());

    return _buffer;
}

bool StopWatch::isRunning() const
{
    return _running;
}

void StopWatch::start()
{
    if(!_running)
    {
        _running = true;
        _clock.restart();
    }
}

void StopWatch::stop()
{
    if(_running)
    {
       // On ajoute dans le buffer le temps écoulé
        _buffer += _clock.getElapsedTime();
        _running = false;
    }
}

void StopWatch::restart(bool stillrunning)
{
    _buffer = sf::Time::Zero;
    _running = stillrunning;
    _clock.restart();
}
