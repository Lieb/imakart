#include "model/Turbo.hpp"
#include "model/Pilot.hpp"

Turbo::Turbo(glm::vec3 coord, typeMesh tMesh, std::string name,  int duration, float scale, int etat, std::string imageGUI):
    Bonus(coord, tMesh, name, duration, scale, etat, imageGUI)
{


    compteRebour = Timer(sf::milliseconds(duration),false);

    anim = glm::mat4(1);

}

Turbo::~Turbo()
{

}

void Turbo::draw()
{

}

void Turbo::active(Pilot *pilot, Pilot *pilotfront)
{
     pilot->kart.velocity *=2.5;
     std::cout << "TURBO STOP" << std::endl;
     pilot->bonus = nullptr;
}
