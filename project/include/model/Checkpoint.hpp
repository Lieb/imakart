/*! \file Checkpoint.hpp
    \brief Implémentations d'une skybox
    \author Clara Baudry, Alexandre Bordereau, Audrey Guénée, Angéline Guignard, Adrien Megueddem, Xavier Vansteene
    \copyright Imakart
    \language C++
    \updated 2013-12-28
*/

#pragma once

#include <string>
#include <glm/glm.hpp>

#include "btBulletDynamicsCommon.h"
#include "graphics/render/Mesh.hpp"

/*! \struct Checkpoint
    Structure d'un Checkpoint
 */
struct Checkpoint
{
    typeMesh tMesh;     /*!< type de mesh : mCheckpoint */
    glm::vec3 coord;    /*!< coordonnées d'un Checkpoint */
    float scale;        /*!< taille/échelle d'un Checkpoint */
    glm::mat4 anim;     /*!< matrice d'animation */

    std::vector<glm::vec3> road;

    Checkpoint(glm::vec3 node, float sc);   /*!< constructeur d'un node */
    ~Checkpoint(); /*!< destructeur d'un node */

    glm::mat4 updateModelView();
};
