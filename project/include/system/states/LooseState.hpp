/*! \file LooseState.hpp
    \brief Etat perdu du jeu qui permet d'annoncer au joueur sa défaite.
    \author Audrey Guénée, Adrien Megueddem
    \copyright Imakart
    \language C++
    \updated 2013-11-24
*/

#pragma once

#include "system/states/GameState.hpp"

#include <stdexcept>

#include <SFML/Graphics.hpp>

#include <TGUI/TGUI.hpp>

#include "graphics/render/Scene.hpp"

class GameStateManager;

class GameWorld;

/*! \struct LooseState
    structure de l'état perdu du jeu.
    singleton pour qu'un état ne soit créé qu'une seule fois et reste unique.
 */
class LooseState : public GameState
{
public:
    virtual void enter();
    virtual void leave();

    virtual void pause();
    virtual void resume();

    virtual void events(GameStateManager& gameStateManager, sf::RenderWindow &window);
    virtual void update(GameStateManager& gameStateManager, sf::RenderWindow &window);
    virtual void draw(sf::RenderWindow &window, int fps);

    static LooseState* instance(sf::RenderWindow& window, GameWorld& model)
    {
        static LooseState loose(window, model);
        return &loose;
    }
protected:
    LooseState(sf::RenderWindow& window, GameWorld& model) : gui(window), model(model)
    {
        // Initialisation de la GUI
        if(gui.setGlobalFont("third-party/TGUI/fonts/run.ttf") == false) throw std::runtime_error("LooseState::Failure to open ttf with TGUI");
        // Load all the widgets from a file
        gui.loadWidgetsFromFile("project/src/graphics/gui/loose.txt");
    }
    GameWorld& model;
    tgui::Gui gui;
    Scene scene;
    std::vector<Pilot*> pilots;
    StopWatch timer;
};
