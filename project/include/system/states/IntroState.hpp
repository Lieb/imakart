/*! \file IntroState.hpp
    \brief Etat d'introduction du jeu.
    \author Audrey Guénée, Adrien Megueddem
    \copyright Imakart
    \language C++
    \updated 2013-11-24
*/

#pragma once

#include "system/states/GameState.hpp"

#include <stdexcept>
#include <iostream>

#include <SFML/Graphics.hpp>
#include <TGUI/TGUI.hpp>

#include "graphics/render.hpp"

#include "model/Stopwatch.hpp"

class GameStateManager;

class GameWorld;

/*! \struct IntroState
    structure de l'état d'introduction du jeu.
    singleton pour qu'un état ne soit créé qu'une seule fois et reste unique.
 */
class IntroState : public GameState
{
public:
    virtual void enter();
    virtual void leave();

    virtual void pause();
    virtual void resume();

    virtual void events(GameStateManager& gameStateManager, sf::RenderWindow &window);
    virtual void update(GameStateManager& gameStateManager, sf::RenderWindow &window);
    virtual void draw(sf::RenderWindow &window, int fps);

    static IntroState* instance(sf::RenderWindow& window, GameWorld& model)
    {
        static IntroState intro(window, model);
        return &intro;
    }

protected:
    IntroState(sf::RenderWindow& window, GameWorld& model) : gui(window), model(model)
    {
        // Initialisation de la Gui
        if(gui.setGlobalFont("third-party/TGUI/fonts/run.ttf") == false) throw std::runtime_error("IntroState::Failure to open ttf with TGUI");
        // Load all the widgets from a file
        gui.loadWidgetsFromFile("project/src/graphics/gui/intro.txt");
    }

    // Déclaration du timer
    StopWatch timer;

    // Référence du model
    GameWorld& model;

    // Graphics
    tgui::Gui gui;
    Scene scene;
    std::vector<Pilot*> pilots;
    //StopWatch translationTimer;
};
