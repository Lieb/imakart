/*! \file Bonus.hpp
    \brief Implémentations des bonus
    \author Clara Baudry, Alexandre Bordereau, Audrey Guénée, Angéline Guignard, Adrien Megueddem, Xavier Vansteene
    \copyright Imakart
    \language C++
    \updated 2013-11-21
*/

#pragma once
#include <iostream>
#include <string>
#include "graphics/render/Mesh.hpp"
#include <glm/glm.hpp>
#include "model/Timer.hpp"
#include <glm/glm.hpp>

struct Pilot;

/*! \class Bonus
    classe des bonus à intercepter dans le jeu
 */
class Bonus
{
public:
    glm::vec3 coord;        /*!< coordonnées du bonus */
    typeMesh tMesh;
    std::string name;
    float scale;
    int duration; /*!< durée sur laquelle s'appliquera le bonus (Millisecond) */
    int etat;
    glm::mat4 anim; /*!< matrice d'animation */
    std::string imageGUI;

    Timer compteRebour;


    Bonus(glm::vec3 coord, typeMesh tMesh, std::string name,  int duration, float scale, int etat,std::string imageGUI);    /*!< constructeur d'un bonus */
    virtual ~Bonus();
    virtual void active(Pilot* pilot, Pilot* pilotfront);
    glm::mat4 updateModelView();

};
