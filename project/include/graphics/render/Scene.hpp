#pragma once

#include <vector>

#include <glm/glm.hpp>

#include "Light.hpp"
#include "TrackballCamera.hpp"
#include "FreeflyCamera.hpp"
#include "Mesh.hpp"

#include "graphics/glimac/Shader.hpp"
#include "graphics/glimac/Program.hpp"

#include "model/GameWorld.hpp"
#include "model/Map.hpp"
#include "model/Pilot.hpp"
#include "model/Bonus.hpp"
#include "model/Decors.hpp"
#include "model/Checkpoint.hpp"

struct Scene
{
    // Shaders
    glimac::Program program;
    glimac::Program skyboxProgram;
    //char fileVs[100] = "./project/src/graphics/shaders/shader.vert";
    //char fileFs[100] = "./project/src/graphics/shaders/shader.frag";

    std::vector<Light> lights;
    Scene();
    ~Scene();

    void render(GameWorld &model);
    void render(GameWorld &model, Map& map);
    void render(GameWorld &model, std::vector<Pilot*>& pilots);
    void render(GameWorld &model, std::vector<Bonus*>& bonus);
    void render(GameWorld &model, std::vector<Decors*>& decors);

    void addLight(const Light& light);
};
