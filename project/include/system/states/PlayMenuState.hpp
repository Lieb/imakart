/*! \file PlayMenuState.hpp
    \brief Etat de pause/menu en cours de jeu qui permet d'afficher un menu en jeu et de mettre pause.
    \author Audrey Guénée, Adrien Megueddem
    \copyright Imakart
    \language C++
    \updated 2013-11-24
*/

#pragma once

#include "system/states/GameState.hpp"

#include <stdexcept>

#include <SFML/Graphics.hpp>

#include <TGUI/TGUI.hpp>

#include "graphics/render/Scene.hpp"

class GameStateManager;

class GameWorld;

/*! \struct PlayMenuState
    structure de l'état de pause/menu du jeu.
    singleton pour qu'un état ne soit créé qu'une seule fois et reste unique.
 */
class PlayMenuState : public GameState
{
public:
    virtual void enter();
    virtual void leave();

    virtual void pause();
    virtual void resume();

    virtual void events(GameStateManager& gameStateManager, sf::RenderWindow &window);
    virtual void update(GameStateManager& gameStateManager, sf::RenderWindow &window);
    virtual void draw(sf::RenderWindow &window, int fps);

    static PlayMenuState* instance(sf::RenderWindow& window, GameWorld& model)
    {
        static PlayMenuState end(window, model);
        return &end;
    }
protected:
    PlayMenuState(sf::RenderWindow& window, GameWorld& model) : gui(window), model(model)
    {
        // nitialisation de la GUI
        if(gui.setGlobalFont("third-party/TGUI/fonts/run.ttf") == false) throw std::runtime_error("PlayMenuState::Failure to open ttf with TGUI");
        // Load all the widgets from a file
        gui.loadWidgetsFromFile("project/src/graphics/gui/playMenu.txt");
    }
    GameWorld& model;
    tgui::Gui gui;
    Scene scene;
};
