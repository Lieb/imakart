#include "model/Bonus.hpp"
#include "model/Pilot.hpp"

Bonus::Bonus(glm::vec3 coord, typeMesh tMesh, std::string name,  int duration, float scale, int etat, std::string imageGUI)
    :coord(coord), tMesh(tMesh), name(name), duration(duration), scale(scale), etat(etat), imageGUI(imageGUI)
{
    compteRebour = Timer(sf::milliseconds(duration),false);
    anim = glm::mat4(1);

}

Bonus::~Bonus()
{
}

glm::mat4 Bonus::updateModelView()
{
    glm::mat4 scaleMatrix = glm::scale(glm::mat4(1.0f), glm::vec3(scale));
    glm::mat4 translateMatrix = glm::translate(glm::mat4(1.0f),coord);
    return translateMatrix * scaleMatrix;
}

void Bonus::active(Pilot *pilot, Pilot *pilotfront){

    std::cout << "coucou je suis virtual active" << std::endl;
}

