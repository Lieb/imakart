#include "model/Swap.hpp"

#include "model/Pilot.hpp"

Swap::Swap(glm::vec3 coord, typeMesh tMesh, std::string name,  int duration, float scale, int etat, std::string imageGUI):
    Bonus(coord, tMesh, name, duration, scale, etat, imageGUI)
{


    compteRebour = Timer(sf::milliseconds(duration),false);

    anim = glm::mat4(1);

}

Swap::~Swap()
{

}

void Swap::draw()
{

}

void Swap::active(Pilot* pilot, Pilot* pilotfront)
{

    // Get var to swap
    glm::vec3 pos1 = pilotfront->kart.getPosition();
    glm::vec3 pos2 = pilot->kart.getPosition();
    glm::vec3 currentNode1 = pilotfront->kart.currentNode;
    glm::vec3 currentNode2 = pilot->kart.currentNode;
    int currentLap1 = pilotfront->kart.currentLap;
    int currentLap2 = pilot->kart.currentLap;
    float distance1 = pilotfront->kart.distanceToCurrentNode;
    float distance2 = pilot->kart.distanceToCurrentNode;

    // swap
    pilot->kart.setPosition(pos1);
    pilotfront->kart.setPosition(pos2);
    pilot->kart.currentNode = currentNode1;
    pilotfront->kart.currentNode = currentNode2;
    pilot->kart.currentLap = currentLap1;
    pilotfront->kart.currentLap = currentLap2;
    pilot->kart.distanceToCurrentNode = distance1;
    pilotfront->kart.distanceToCurrentNode = distance2;

    pilot->bonus = nullptr;

}
