/*! \file GameWorld.hpp
    \brief Implémentations du monde.
    \author Clara Baudry, Alexandre Bordereau, Audrey Guénée, Angéline Guignard, Adrien Megueddem, Xavier Vansteene
    \copyright Imakart
    \language C++
    \updated 2013-12-16
*/

#pragma once
#include <vector>
#include <map>
#include <iostream>
#include <cstdlib>

#include <SFML/Audio.hpp>

#include "graphics/render/Mesh.hpp"
#include "graphics/render/Camera.hpp"
#include "graphics/render/TrackballCamera.hpp"
#include "graphics/render/FreeflyCamera.hpp"

#include "btBulletDynamicsCommon.h"
#include "graphics/debug/GLDebugDrawer.hpp"

#include "model/Map.hpp"
#include "model/Bonus.hpp"
#include "model/Kart.hpp"
#include "model/Pilot.hpp"
#include "model/Decors.hpp"

#include "model/Skybox.hpp"

#include "model/Checkpoint.hpp"

/*! \struct GameWorld
    structure du monde (moteur de jeu)
 */
struct GameWorld
{
    std::map<typeMesh,Mesh*> meshs;
    Skybox skybox;
    Map map;
    std::vector<Pilot*> pilots;
    std::vector<Pilot*> positions;
    std::vector<Checkpoint*> checkpoint;

    // Sounds
    //  -   Musics
    sf::Music configs;
    sf::Music menus;
    sf::Music sand;
    sf::Music grass;
    sf::Music credits;
    //  -   Effects
    
    // buffer des sounds
    sf::SoundBuffer kart_b;
    sf::SoundBuffer click_b;
    sf::SoundBuffer catchBonus_b;
    sf::SoundBuffer win_b;
    sf::SoundBuffer loose_b;
    sf::SoundBuffer rallye_b;

    // sons
    sf::Sound kart;
    sf::Sound click;
    sf::Sound catchBonus;
    sf::Sound win;
    sf::Sound loose;
    sf::Sound rallye;



    // Camera
    //TrackballCamera camera;
    Camera* camera;
    TrackballCamera camera1;
    FreeflyCamera camera2;
    //bool viewCamera;

    // physique
    btBroadphaseInterface* broadphase;     /*!< fournit une interface pour détecter où les objets et leurs AABB se chevauchent */
    btDefaultCollisionConfiguration* collisionConfiguration;
    btCollisionDispatcher* dispatcher;
    btSequentialImpulseConstraintSolver* solver;
    btDiscreteDynamicsWorld *dynamicsWorld;
    GLDebugDrawer debugDrawer;
    bool debugActive;

    GameWorld();    /*!< constructeur du monde */
    ~GameWorld();   /*!< destructeur du monde */

    void initWorld();
    void initSounds();
    void initMeshs();
    void initMap();
    void initPilots();
    void initPositions();
    void initCamera();
    void updateRank();
    int getIndexCheckpoint(glm::vec3 coord);
    void updateCurrentNode(Kart &kart);
    int endGame(Kart &kart);

    void switchCamera();

    void updateAttractionForces();
    void addObjectToDynamicWorld();
    void updateMatrixPhysicKarts();
    void updateMatrixPhysicDecors();
    Pilot* getPilotFrontMe(int pilotPos);
    void addBonusToPilots();
};
